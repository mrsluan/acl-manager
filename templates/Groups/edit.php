<?php
/**
 * @var \App\View\AppView $this
 */

$controller = $this->request->getParam('controller');
$action = $this->request->getParam('action');

$this->PluginPageHeader->addTitle(['label' => 'Group', 'icon' => 'fa fa-pencil']);
$this->PluginPageHeader->addSubTitle(['label' => 'Edit Group']);

$this->Breadcrumbs->add('<i class="fa fa-dashboard"></i> Dashboard', ['controller' => 'Dashboards'], ['escape' => false]);
$this->Breadcrumbs->add('<i class="fa fa-bars"></i> List Groups', ['action' => 'index'], ['escape' => false]);
$this->Breadcrumbs->add('<i class="fa fa-pencil"></i> Edit Group', null);
$this->Breadcrumbs->add('<i class="fa fa-question-circle"></i> Help', '#', ['onclick' => "showHelper( '{$controller}', '{$action}')", 'escape' => false]);

?>

<section class="container-fluid">
    <div class="row">
        <div class="box box-primary">
            <div class="box-header">
                <legend><?= __('Edit Group') ?></legend>
            </div>
            <div class="box-body">
                <?= $this->Form->create($group); ?>

                <div class="row">
                    <div class="form-group col-md-6">
                        <h3><?= $group->id ?></h3>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-6">
                        <?php
                        echo $this->Form->control('alias', array(
                            'label' => 'Group Name (Alias)',
                            'class' => 'form-control text-uppercase',
                        ));
                        ?>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-6">
                        <?php
                        echo $this->Form->control('model', array(
                            'label' => 'Model',
                            'class' => 'form-control',
                        ));
                        ?>
                    </div>
                    <div class="form-group col-md-3">
                        <?php
                        echo $this->Form->control('foreign_key', array(
                            'label' => 'Foreign Key',
                            'class' => 'form-control',
                        ));
                        ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-5 col-md-offset-2">
                        <?= $this->Form->button(__('<i class="fa fa-save"></i> Save'), [
                            'class' => "btn btn-success",
                            'escapeTitle' => false,
                        ]); ?>
                        <?= $this->Form->button(__('<i class="fa fa-ban"></i> Cancel'), [
                            'type' => 'reset',
                            'class' => "btn btn-danger",
                            'escapeTitle' => false,
                        ]); ?>
                    </div>
                </div>
                <?php //= $this->Form->button(__('Submit')) ?>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</section>
