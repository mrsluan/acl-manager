<?php
/**
 * @var \App\View\AppView $this
 */

$controller = $this->request->getParam('controller');
$action = $this->request->getParam('action');

$this->PluginPageHeader->addTitle(['label' => 'Group', 'icon' => 'fa fa-users']);
$this->PluginPageHeader->addSubTitle(['label' => 'Group Detail']);

$this->Breadcrumbs->add('<i class="fa fa-dashboard"></i> Dashboard', ['controller' => 'Dashboards'], ['escape' => false]);
$this->Breadcrumbs->add('<i class="fa fa-bars"></i> List Groups', ['action' => 'index'], ['escape' => false]);
$this->Breadcrumbs->add('<i class="fa fa-search"></i> Group Detail', null);
$this->Breadcrumbs->add('<i class="fa fa-question-circle"></i> Help', '#', ['onclick' => "showHelper( '{$controller}', '{$action}')", 'escape' => false]);
?>


<section class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-success">
                <div class="box-body">

                    <div class="row form-group">
                        <p class="col-md-2 text-left"><strong><?= __('Id') ?></strong></p>
                        <p class="col-md-10"><?= h($group->id) ?></p>
                    </div>
                    <div class="row form-group">
                        <p class="col-md-2 text-left"><strong><?= __('Group Name') ?></strong></p>
                        <p class="col-md-10"><?= h($group->alias) ?></p>
                    </div>
                    <div class="row form-group">
                        <p class="col-md-2 text-left"><strong><?= __('Model') ?></strong></p>
                        <p class="col-md-10"><?= h($group->model) ?></p>
                    </div>
                    <div class="row form-group">
                        <p class="col-md-2 text-left"><strong><?= __('Foreign Key (Alias)') ?></strong></p>
                        <p class="col-md-10"><?= h($group->foreign_key) ?></p>
                    </div>
                </div>

                <div class="box-footer">
                    <div class="col-sm-6 col-md-3">
                        <?php
                        echo $this->Html->link(
                            $this->Form->button('<i class="fa fa-pencil"></i> Edit Group', [
                                'class' => 'btn btn-block btn-primary',
                                'type' => 'button',
                                'escapeTitle' => false,
                            ]), ['action' => 'edit', $group->id], ['escape' => false]);
                        ?>
                    </div>

                    <div class="col-sm-6 col-md-3">
                        <?php
                        echo $this->Html->link(
                            $this->Form->button('<i class="fa fa-bars"></i> List Group', [
                                'class' => 'btn btn-block btn-primary',
                                'type' => 'button',
                                'escapeTitle' => false,
                            ]), ['action' => 'index'], ['escape' => false]);
                        ?>

                    </div>
                    <div><br/><br/></div>
                </div>
            </div>
        </div>
    </div>
</section>
