<?php
/**
 * @var \App\View\AppView $this
 * @var \AclManager\Model\Entity\Group[] $groups
 */

$controller = $this->request->getParam('controller');
$action = $this->request->getParam('action');

$this->PluginPageHeader->addTitle(['label' => 'Groups', 'icon' => 'fa fa-bars']);
$this->PluginPageHeader->addSubTitle(['label' => 'List Groups']);

$this->Breadcrumbs->add('<i class="fa fa-dashboard"></i> Dashboard', ['controller' => 'Dashboards'], ['escape' => false]);
$this->Breadcrumbs->add('<i class="fa fa-bars"></i> List Groups', null);
$this->Breadcrumbs->add('<i class="fa fa-question-circle"></i> Help', '#', ['onclick' => "showHelper( '{$controller}', '{$action}')", 'escape' => false]);
?>

<section class="container-fluid">
    <div class="row">
        <article class="col-md-12 col-xs-12">
            <div class="box box-info">
                <div class="box-header">
                    <div class="row">
                        <h4 class="box-title col-xs-8">
                            <i class="fa fa-filter"></i> Search
                        </h4>
                        <p class="col-xs-4 text-right h4">
                            <a class="text-primary" data-toggle="collapse" href="#collapseExample"
                               aria-expanded="true" aria-controls="collapseExample">
                                <i class="fa fa-plus-square fa-lg"></i>
                            </a>
                        </p>
                    </div>
                </div>
                <div class="box-body collapse" id="collapseExample">
                    <?= $this->Form->create(null, [
                        'type' => 'get',
                        'valueSources' => 'query'
                    ]); ?>
                    <div class="row">
                        <div class="form-group col-xs-12 col-md-6">
                            <?= $this->Form->control('search.name', [
                                'label' => 'Group Name',
                                'style' => 'text-transform:uppercase',
                                'type' => 'text',
                                'placeholder' => 'Group Name',
                            ]); ?>
                        </div>
                        <div class="form-group col-xs-12 text-right">
                            <?= $this->Form->button('Find', [
                                'class' => 'btn btn-success',
                                'type' => 'submit',
                            ]); ?>
                            <?= $this->Html->link(
                                $this->Form->button('Clear', [
                                    'class' => 'btn btn-danger',
                                    'type' => 'button',
                                ]),
                                ['controller' => 'groups', 'action' => 'index'],
                                ['escape' => false]
                            ); ?>
                        </div>
                    </div>
                    <?= $this->Form->end(); ?>
                </div>
            </div>
        </article>
        <article class="col-xs-12">
            <div class="box box-primary">
                <div class="box-header">
                    <?= $this->PluginAcl->link(__('New Group'), ['action' => 'add'], ['class' => 'btn btn-primary']); ?>
                    <hr>
                </div>
                <div class="box-body">
                    <div class=" table-responsive">
                        <table class="table table-bordered table-hover table-striped table-condensed bg-white">
                            <thead>
                            <tr>
                                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                                <th scope="col"><?= $this->Paginator->sort('alias', 'Group Name (Alias)') ?></th>
                                <th scope="col"><?= $this->Paginator->sort('model', 'Model') ?></th>
                                <th scope="col"><?= $this->Paginator->sort('foreign_key', 'Foreign Key') ?></th>
                                <th scope="col" class="actions"><?= __('Actions') ?></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $_class = 0;

                            foreach ($groups as $group):
                                ?>
                                <tr class="<?= $_class++ % 2 ? 'odd' : 'even' ?>" role="row">
                                    <td><?= h($group->id); ?>&nbsp;</td>
                                    <td><?= h($group->alias); ?>&nbsp;</td>
                                    <td><?= h($group->model); ?>&nbsp;</td>
                                    <td><?= h($group->foreign_key); ?>&nbsp;</td>

                                    <td class="actions">
                                        <?= $this->PluginAcl->link('<i class="fa fa-search"></i>', ['action' => 'view', $group->id], ['class' => 'btn btn-primary btn-xs', 'title' => "View", 'escape' => false]) ?>
                                        <?= $this->PluginAcl->link('<i class="fa fa-pencil"></i>', ['action' => 'edit', $group->id], ['class' => 'btn btn-success btn-xs', 'title' => "Edit", 'escape' => false]) ?>
                                        <?= $this->PluginAcl->postLink('<i class="fa fa-trash-o"></i>', ['action' => 'delete', $group->id], ['confirm' => __('Are you sure you want to delete Group # {0}?', $group->alias), 'class' => 'btn btn-danger btn-xs', 'title' => "Excluir", 'escape' => false]) ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="row">
                        <nav class="col-md-12 text-center">
                            <ul class="pagination">
                                <?= $this->Paginator->numbers(['first' => '<< Primeira', 'prev' => '< Anterior', 'next' => 'Próximo >', 'last' => 'Última >>']) ?>
                            </ul>
                        </nav>
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <?= $this->Paginator->counter('Página {{page}} de {{pages}} ({{current}} de {{count}} registros).'); ?>
                        </div>
                    </div>
                </div>
            </div>
        </article>
    </div>
</section>
