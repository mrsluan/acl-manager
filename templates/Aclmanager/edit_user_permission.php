<?php
//$this->Html->script('Ntiaclmanager.aclmanager', ['block' => true]);

$groupName = isset($user->gr['name']) && $user->gr['name'] != '' ? $user->gr['name'] : '' ?>

<h1>
    ACL Manager
    <small>User Permission</small>
</h1>

<div class="container">
    <div class="row">
        <div class="col-md-9">

            <?php if ($app['app']): ?>
                <div class="row">
                    <div class="col-md-12">
                        <div class="box box-solid box-default">
                            <div class="box-header">
                                <h3 class="box-title"><i class="fa fa-gears"></i> App Controller Permission</h3>
                            </div><!-- /.box-header -->
                            <div class="box-body">
                                <?php foreach ($app['app']['controllers'] as $controllerName => $controller): ?>

                                    <div style="margin-bottom: 2px;" class="box box-solid box-default collapsed-box">
                                        <div class="box-header with-border">
                                            <h3 class="box-title"><?= $controllerName ?> Controller</h3>
                                            <div class="box-tools pull-right">
                                                <button name="app<?= $controllerName; ?>" class="showActions"
                                                        class="btn btn-box-tool" data-widget="collapse"><i
                                                        class="fa fa-plus"></i></button>
                                            </div><!-- /.box-tools -->
                                        </div><!-- /.box-header -->
                                        <div class="box-body">
                                            <div class="col-xs-12">
                                                <div class="box">
                                                    <div class="box-body table-responsive no-padding">
                                                        <table name="table_app<?= $controllerName; ?>"
                                                               class="table table-hover">
                                                            <tbody>
                                                            <tr>
                                                                <th style="width:20%">Action</th>
                                                                <?php foreach ($columns as $column): ?>
                                                                    <th style="text-align:center"><?= $column ?></th>
                                                                <?php endforeach; ?>
                                                            </tr>
                                                            <?php
                                                            // $controllerName .= $controllerName.'fdf';
                                                            foreach ($controller['actions'] as $action):
                                                                ?>
                                                                <tr>
                                                                    <?php if (in_array("controllers\\$controllerName\\$action", $acosPath)): ?>
                                                                        <td><?= $action ?></td>
                                                                        <?php
                                                                        foreach ($columns as $column):
                                                                            $json = "[['$groupName/$user->name'],['$controllerName'],['$action'],['$column']]";
                                                                            ?>
                                                                            <td align="center">
                                                                                <a class="permission"
                                                                                   name="<?= $json ?>" href="#">
                                                                                    <input type="hidden"
                                                                                           id="<?= $json ?>"
                                                                                           value="load"/>
                                                                                    <i style="color: orange;font-size: large "
                                                                                       class="fa  fa-spinner fa-spin"></i>
                                                                                </a>
                                                                            </td>
                                                                        <?php endforeach; ?>
                                                                    <?php else: ?>
                                                                        <td><span
                                                                                style="color:red; text-decoration:line-through;"><span
                                                                                    style="color: black"><?= $action ?></span></span>
                                                                        </td>
                                                                        <?php
                                                                        foreach ($columns as $column):
                                                                            $json = "[['$groupName/$user->name'],['$controllerName'],['$action'],['$column']]";
                                                                            ?>
                                                                            <td align="center">
                                                                                <a href="#" data-toggle="tooltip"
                                                                                   data-original-title="Action must be synchronized">
                                                                                    <input type="hidden"
                                                                                           id="<?= $json ?>"
                                                                                           value="notSynchronize"/>
                                                                                    <i style="color: #FFC600;font-size: large "
                                                                                       class="fa  fa-warning"></i>
                                                                                </a>
                                                                            </td>
                                                                        <?php endforeach; ?>
                                                                    <?php endif; ?>
                                                                </tr>
                                                            <?php endforeach; ?>
                                                            </tbody>
                                                        </table>
                                                    </div><!-- /.box-body -->
                                                </div><!-- /.box -->
                                            </div>
                                        </div><!-- /.box-body -->
                                    </div><!-- /.box -->
                                <?php endforeach; ?>
                            </div><!-- /.box-body -->
                        </div><!-- /.box -->
                    </div>
                </div>
            <?php endif; ?>

            <?php if ($app['plugins']): ?>
                <div class="row">
                    <div class="col-md-12">
                        <div class="box box-solid box-default">
                            <div class="box-header">
                                <h3 class="box-title"><i class="fa fa-gears"></i> Plugins Permission</h3>
                            </div><!-- /.box-header -->
                            <div class="box-body">
                                <?php foreach ($app['plugins'] as $pluginName => $controllers): ?>
                                    <div style="margin-bottom: 2px;" class="box box-solid box-default collapsed-box">
                                        <div class="box-header with-border">
                                            <h3 class="box-title"><?= $pluginName ?> Plugin</h3>
                                            <div class="box-tools pull-right">
                                                <button name="plugin<?= $controllerName; ?>" class="btn btn-box-tool"
                                                        data-widget="collapse"><i class="fa fa-plus"></i></button>
                                            </div><!-- /.box-tools -->
                                        </div><!-- /.box-header -->
                                        <div class="box-body">

                                            <?php foreach ($controllers['controllers'] as $controllerName => $controller): ?>

                                                <div style="margin: 2px;"
                                                     class="box box-solid box-default collapsed-box">
                                                    <div class="box-header with-border">
                                                        <h3 class="box-title"><?= $controllerName; ?></h3>
                                                        <div class="box-tools pull-right">
                                                            <button name="plugin<?= $controllerName; ?>"
                                                                    class="showActions" class="btn btn-box-tool"
                                                                    data-widget="collapse"><i class="fa fa-plus"></i>
                                                            </button>
                                                        </div><!-- /.box-tools -->
                                                    </div><!-- /.box-header -->
                                                    <div class="box-body ">

                                                        <div class="col-xs-12">
                                                            <div class="box">
                                                                <div class="box-body table-responsive no-padding">
                                                                    <table name="table_plugin<?= $controllerName; ?>"
                                                                           class="table table-hover">
                                                                        <!-- table class="table table-hover" -->
                                                                        <tbody>
                                                                        <tr>
                                                                            <th style="width:20%">Action</th>
                                                                            <?php foreach ($columns as $column): ?>
                                                                                <th style="text-align:center"><?= $column ?></th>
                                                                            <?php endforeach; ?>
                                                                        </tr>
                                                                        <?php foreach ($controller['actions'] as $action): ?>
                                                                            <tr>
                                                                                <?php if (in_array("controllers\\$pluginName\\$controllerName\\$action", $acosPath)): ?>
                                                                                    <td><?= $action ?></td>
                                                                                    <?php
                                                                                    foreach ($columns as $column):
                                                                                        $json = "[['$groupName/$user->name'],['$pluginName'],['$controllerName'],['$action'],['$column']]";
                                                                                        ?>
                                                                                        <td align="center">

                                                                                            <a class="permission"
                                                                                               name="<?= $json ?>"
                                                                                               href="#">
                                                                                                <input type="hidden"
                                                                                                       id="<?= $json ?>"
                                                                                                       value="load"/>
                                                                                                <i style="color: orange;font-size: large "
                                                                                                   class="fa  fa-spinner fa-spin"></i>
                                                                                            </a>
                                                                                        </td>
                                                                                    <?php endforeach; ?>
                                                                                <?php else: ?>
                                                                                    <td><span
                                                                                            style="color:red; text-decoration:line-through;"><span
                                                                                                style="color: black"><?= $action ?></span></span>
                                                                                    </td>
                                                                                    <?php
                                                                                    foreach ($columns as $column):
                                                                                        $json = "[['$groupName/$user->name'],['$pluginName'],['$controllerName'],['$action'],['$column']]";
                                                                                        ?>
                                                                                        <td align="center">
                                                                                            <a href="#"
                                                                                               data-toggle="tooltip"
                                                                                               data-original-title="Action must be synchronized">
                                                                                                <input type="hidden"
                                                                                                       id="<?= $json ?>"
                                                                                                       value="notSynchronize"/>
                                                                                                <i style="color: #FFC600;font-size: large "
                                                                                                   class="fa  fa-warning"></i>
                                                                                            </a>
                                                                                        </td>
                                                                                    <?php endforeach; ?>
                                                                                <?php endif; ?>
                                                                            </tr>

                                                                        <?php endforeach; ?>

                                                                        </tbody>
                                                                    </table>
                                                                </div><!-- /.box-body -->
                                                            </div><!-- /.box -->
                                                        </div>
                                                    </div><!-- /.box-body -->
                                                </div><!-- /.box -->
                                            <?php endforeach; ?>

                                        </div><!-- /.box-body -->
                                    </div><!-- /.box -->
                                <?php endforeach; ?>
                            </div><!-- /.box-body -->
                        </div><!-- /.box -->
                    </div>
                </div>
            <?php endif; ?>
            <!-- path to check permission, using into jquery ajax. -->
            <input id="url" type="hidden" value=".."/>
        </div>
        <div class="col-md-3">
            <!--            <div class="span3">-->

            <div class="well">
                <fieldset class="adminform">
                    <legend>User <span style="color: blue">( Group <?= $groupName ?> ) </span></legend>
                    <i style="font-size: xx-large; float: right;color: #3C8DBC; margin: 0 0 0 10px;"
                       class="glyphicon glyphicon-user"></i>
                    <dl class="info">
                        <dt>Name</dt>
                        <dd><?= $user->firstname . ' ' . $user->lastname ?></dd>
                        <dt>Login Name</dt>
                        <dd><?= $user->name ?></dd>
                        <dt>Email</dt>
                        <dd><?= $user->email ?></dd>
                        <dt>Registration</dt>
                        <dd><?= $user->created ?></dd>
                        <dt>Last Visit</dt>
                        <dd><?= $user->lastaccess ?></dd>
                        <dt>User ID</dt>
                        <dd><?= $user->id ?></dd>
                    </dl>
                </fieldset>
            </div>

            <div class="well">
                <fieldset class="adminform">
                    <legend>Legend</legend>
                    <ul style="list-style-type: none">
                        <li class="rule">
                            <i class="fa fa-check" style="color: green;font-size: medium"></i>
                            <span class="legend hasTip" title=""> Allowed</span>
                        </li>
                        <li class="rule">
                            <i class="fa fa-close" style="color: red;font-size: medium"></i>
                            <span class="legend hasTip" title=""> Denied</span>
                        </li>
                        <li class="rule">
                            <i class="fa  fa-warning" style="color: #FFC600;font-size: medium "></i>
                            <span class="legend hasTip" title=""> Failured</span>
                        </li>
                    </ul>
                </fieldset>
            </div>

            <div class="clr"></div>        <!-- /div -->
        </div>
    </div>

</div>
