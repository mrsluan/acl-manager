<?php
/**
 * @var \App\View\AppView $this
 */
?>
<h1>
    Users
    <small><?= $option != 'all' ? __('Users {0}', \Cake\Utility\Inflector::camelize($option)) : 'Users System' ?></small>
</h1>

<!-- FORM SEARCH -->
<div class="box box-solid box-default">
    <div class="box-header with-border">
        <h3 class="box-title">Filter</h3>
        <div class="box-tools pull-right">
            <button class="btn btn-box-tool" data-widget="collapse"><i class="glyphicon glyphicon-minus-sign"></i>
            </button>
        </div><!-- /.box-tools -->
    </div><!-- /.box-header -->
    <div class="box-body">
        <?= $this->Form->create(null, ['type' => 'get', 'role' => 'form']); ?>
        <div class="box-body">
            <div class="col-xs-4">
                <?= $this->Form->control('search.name', ['label' => false, 'type' => 'text', 'placeholder' => 'User Name', 'class' => 'form-control', 'value' => $search['name']]); ?>
            </div>
            <div class="col-xs-3">
                <div class="input-group">
                    <div class="input-group-addon">
                        <i class="glyphicon glyphicon-calendar"></i>
                    </div>
                    <?= $this->Form->control('search.fromCreateDate', ['label' => false, 'type' => 'text', 'data-mask' => '', 'placeholder' => 'From Created', 'data-inputmask' => "'alias': 'dd/mm/yyyy'", 'class' => 'form-control', 'value' => $search['fromCreateDate']]); ?>
                </div><!-- /.input group -->
            </div>
            <div class="col-xs-3">
                <div class="input-group">
                    <div class="input-group-addon">
                        <i class="glyphicon glyphicon-calendar"></i>
                    </div>
                    <?= $this->Form->control('search.toCreateDate', ['label' => false, 'type' => 'text', 'data-mask' => '', 'placeholder' => 'To Created', 'data-inputmask' => "'alias': 'dd/mm/yyyy'", 'class' => 'form-control', 'value' => $search['toCreateDate']]); ?>
                </div><!-- /.input group -->
            </div>
        </div><!-- /.box-body -->

        <div class="box-footer">
            <div class="col-sm-6 col-md-3">
                <?= $this->Form->button($this->Html->tag('i', '', ['class' => 'glyphicon glyphicon-search']) . 'Pesquisar',
                    ['class' => 'btn btn-block btn-default', 'type' => 'sumbit']); ?>

            </div>
            <div class="col-sm-6 col-md-3">

                <?php
                echo $this->Html->link(
                    $this->Form->button('<i class="glyphicon glyphicon-plus"></i> New User',
                        ['class' => 'btn btn-block btn-default', 'type' => 'button']), ['controller' => 'Users', 'action' => 'add'], ['escape' => false]);
                ?>

            </div>
            <div class="col-sm-6 col-md-3">
                <?php
                echo $this->Html->link(
                    $this->Form->button('<i class="glyphicon glyphicon-list"></i> List Groups',
                        ['class' => 'btn btn-block btn-default', 'type' => 'button']), ['controller' => 'Groups'], ['escape' => false]);
                ?>


            </div>
            <div class="col-sm-6 col-md-3">
                <button class="btn btn-block btn-default" type="button"><i class="glyphicon glyphicon-plus"></i> New
                    Group
                </button>
            </div>
        </div>
        <!-- /form -->
        <?= $this->Form->end() ?>
    </div><!-- /.box-body -->
</div><!-- /.box -->


<!-- TABLE RESULT -->
<div class="box box-solid box-default">
    <div class="box-header">
        <h3 class="box-title">User List</h3>
    </div><!-- /.box-header -->
    <div class="box-body">


        <div class="dataTables_wrapper form-inline dt-bootstrap" id="example1_wrapper">
            <div class="row">
                <div class="col-sm-12">
                    <table aria-describedby="example1_info" role="grid" id="example1"
                           class="table table-bordered table-striped dataTable">
                        <thead>
                        <tr role="row">

                            <th aria-sort="descending" aria-label="Browser: activate to sort column ascending"
                                style="width: 70px;" colspan="1" rowspan="1" aria-controls="example1" tabindex="0"
                                class="sorting_desc"><?= $this->Paginator->sort('id'); ?></th>

                            <th aria-sort="descending" aria-label="Browser: activate to sort column ascending"
                                style="width: 207px;" colspan="1" rowspan="1" aria-controls="example1" tabindex="0"
                                class="sorting_desc"><?= $this->Paginator->sort('name', 'User Name'); ?></th>

                            <th aria-sort="descending" aria-label="Browser: activate to sort column ascending"
                                style="width: 207px;" colspan="1" rowspan="1" aria-controls="example1" tabindex="0"
                                class="sorting_desc"><?= $this->Paginator->sort('firstname', 'Name'); ?></th>

                            <th aria-sort="descending" aria-label="Browser: activate to sort column ascending"
                                style="width: 207px;" colspan="1" rowspan="1" aria-controls="example1" tabindex="0"
                                class="sorting_desc"><?= $this->Paginator->sort('status'); ?></th>
                            <th aria-label="Rendering engine: activate to sort column ascending" style="width: 162px;"
                                colspan="1" rowspan="1" aria-controls="example1" tabindex="0"
                                class="sorting"><?= $this->Paginator->sort('lastaccess', 'Last Access'); ?></th>

                            <th aria-label="Rendering engine: activate to sort column ascending" style="width: 162px;"
                                colspan="1" rowspan="1" aria-controls="example1" tabindex="0"
                                class="actions sorting"><?= __('Actions'); ?></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $_class = 0;

                        foreach ($users as $user):
                            $status = $user->status == 1 ? 'Active' : 'Inactive';
                            $color = $user->status == 1 ? 'blue' : 'red';
                            ?>
                            <tr class="<?= $_class++ % 2 ? 'odd' : 'even' ?>" role="row">
                                <td><?= h($user->id); ?>&nbsp;</td>
                                <td><?= h($user->name); ?>&nbsp;</td>
                                <td><?= h($user->firstname . ' ' . $user->lastname); ?>&nbsp;</td>
                                <td style="color: <?= $color ?>"><?= h($status); ?>&nbsp;</td>
                                <td><?= h($user->lastaccess); ?>&nbsp;</td>
                                <td class="actions" style="text-align: center">
                                    <?= $this->Html->link('<i style="font-size: large;" class="fa fa-unlock-alt"></i>',
                                        ['action' => 'editUserPermission', $user->id], ['escape' => false]); ?>
                                </td>


                            </tr>
                        <?php endforeach; ?>

                        </tbody>
                        <tfoot>
                        <tr>
                            <th colspan="1" rowspan="1">Id</th>
                            <th colspan="1" rowspan="1">User Name</th>
                            <th colspan="1" rowspan="1">Name</th>
                            <th colspan="1" rowspan="1">Status</th>
                            <th colspan="1" rowspan="1">Last Access</th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-5">
                    <div aria-live="polite" role="status" id="example1_info" class="dataTables_info">

                        <?php

                        $page = $this->Paginator->counter('{{page}}');
                        $pages = $this->Paginator->counter('{{pages}}');
                        echo $this->Paginator->counter('Showing {{page}} to {{pages}}, {{count}} entries');

                        ?>

                    </div>
                </div>
                <div class="col-sm-7">
                    <div id="example1_paginate" class="dataTables_paginate paging_simple_numbers">
                        <ul class="pagination">


                            <li class="paginate_button previous disabled" id="example1_previous">
                                <?= $this->Paginator->prev('Previous'); ?>
                            </li>

                            <li class="paginate_button">
                                <?= $this->Paginator->numbers() ?>
                            </li>
                            <li class="paginate_button next" id="example1_next">
                                <?= $this->Paginator->next('Next'); ?>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- /.box-body -->

</div>

<!-- FIM -->
