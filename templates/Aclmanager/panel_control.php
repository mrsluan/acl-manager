<?php
$this->PluginPageHeader->addTitle(['label' => 'ACL Manager', 'icon' => 'fa fa-gear']);
$this->PluginPageHeader->addSubTitle(['label' => 'Panel Control']);

$this->Breadcrumbs->add('<i class="fa fa-dashboard"></i> Dashboard', ['controller' => 'Dashboards'], ['escape' => false]);
$this->Breadcrumbs->add('<i class="fa fa-gear"></i> Panel Control', null);
$this->Breadcrumbs->add('<i class="fa fa-question-circle"></i> Help', '#', ['onclick' => "showHelper( 'Pages', 'display')", 'escape' => false]);
?>

<section class="container-fluid">
    <div class="row">
        <article class="col-md-12 col-xs-12">
            <div class="box box-info">
                <div class="box-body">

                    <div class="col-md-4">

                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <h3 class="panel-title">List Groups</h3>
                            </div><!-- /.box-header -->
                            <div class="panel-body">

                                <table class="table table-condensed">
                                    <tbody>
                                    <tr>
                                        <th>Group Title</th>
                                        <th style="width: 10%">ID</th>
                                    </tr>
                                    <?php
                                    foreach ($groups as $id => $group) {
                                        echo "<tr>";
                                        echo "<td>$group</td>";
                                        echo "<td>$id</td>";
                                        echo "</tr>";
                                    }
                                    ?>
                                    </tbody>
                                </table>

                            </div><!-- /.panel-body -->
                        </div><!-- /.panel -->
                    </div>

                    <div class="col-md-5">
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <h3 class="panel-title">Diagnostic Checks</h3>
                            </div><!-- /.box-header -->
                            <div class="panel-body">
                                <div>
                                    <i style="font-size: xx-large; float: right;color: #F0AD4E; margin: 0 0 0 10px;"
                                       class="fa fa-wrench"></i>

                                    <?php $css = $orphanPermission ? 'style="color: red" class="fa fa-exclamation-triangle"' : 'style="color: green" class="fa fa-check-circle"'; ?>
                                    <ul style="list-style-type: none;padding-left:0px">
                                        <li>
                                            <i <?= $css ?> ></i>&nbsp;&nbsp; <b>Orphan Assets</b> ( Have into the Table
                                            Aco but haven't into System )
                                        </li>
                                        <?php $css = $lackPermission ? 'style="color: red" class="fa fa-exclamation-triangle"' : 'style="color: green" class="fa fa-check-circle"'; ?>

                                        <li>
                                            <i <?= $css ?> ></i>&nbsp;&nbsp; <b>Missing Assets</b> ( Have into Sytem but
                                            haven't into Table Aco )
                                        </li>

                                        <li class="error">
                                            &nbsp;
                                        </li>

                                        <li class="success">
                                            <i style="color: red" class="fa fa-check"></i>
                                            <?= $this->Html->link(__('Click to Update'), ['action' => 'panelControl', 'update']) ?>
                                        </li>
                                        <li class="success">
                                            <i style="color: red" class="fa fa-check"></i>
                                            <?= $this->Html->link(__('Click to Sync'), ['action' => 'panelControl', 'sync']) ?>
                                        </li>
                                        <li class="success">
                                            <i style="color: red" class="fa fa-check"></i>
                                            <?= $this->Html->link(__('Click to Recover Aco'), ['action' => 'panelControl', 'recover', 'aco']) ?>
                                        </li>

                                        <li class="success">
                                            <i style="color: red" class="fa fa-check"></i>
                                            <?= $this->Html->link(__('Click to Recover Aro'), ['action' => 'panelControl', 'recover', 'aro']) ?>
                                        </li>

                                    </ul>
                                </div>
                            </div><!-- /.panel-body -->
                        </div><!-- /.panel -->
                    </div>

                    <div class="col-md-3">
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <h3 class="panel-title"> ACL Manager</h3>
                            </div><!-- /.box-header -->
                            <div class="panel-body">
                                <div class="well">
                                    <i class="fa fa-lock"><span> Acl Manager</span></i>
                                    <dl>
                                        <dt>Version</dt>
                                        <dd>3.0</dd>
                                        <dt>Released</dt>
                                        <dd>20/05/2024</dd>
                                        <dt>Support</dt>
                                        <dd><a target="_blank" href="">mrsluan@gmail.com</a></dd>
                                        <dt>CakePHP 4.5</dt>
                                    </dl>
                                </div>
                            </div><!-- /.panel-body -->
                        </div><!-- /.panel -->
                    </div>

                    <br/><br/><br/><br/>
                    <br/><br/><br/><br/>
                    <br/><br/><br/><br/>
                    <br/><br/><br/><br/>
                    <br/><br/><br/><br/>
                </div>
            </div>
        </article>

    </div>
</section>
