<?php
/**
 * @var \App\View\AppView $this
 */
//dd($this->request->getAttribute('csrfToken'));

$this->Html->script('AclManager.aclmanager', ['block' => true]);

$this->PluginPageHeader->addTitle(['label' => 'ACL Manager', 'icon' => 'fa fa-gears']);
$this->PluginPageHeader->addSubTitle(['label' => 'Group Permission']);

$this->Breadcrumbs->add('<i class="fa fa-dashboard"></i> Dashboard', ['controller' => 'Dashboards'], ['escape' => false]);
$this->Breadcrumbs->add('<i class="fa fa-gears"></i> Group Permission', null);
$this->Breadcrumbs->add('<i class="fa fa-question-circle"></i> Help', '#', ['onclick' => "showHelper( 'Pages', 'display')", 'escape' => false]);
?>

<?php if ($app['app']): ?>
    <div class="row">
        <div class="col-md-12">
            <div class="box box-solid box-default">
                <div class="box-header">
                    <h3 class="box-title"><i class="fa fa-gears"></i> App Controller Permission</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <?php foreach ($app['app']['controllers'] as $controllerName => $controller): ?>

                        <div class="box box-info">
                            <h4 class="box-header">
                                <div class="row">
                                    <a class="text-primary col-md-12 col-xs-11 callShowAction" data-toggle="collapse"
                                       href="#collapseApp<?= $controllerName; ?>" aria-expanded="true"
                                       aria-controls="collapseExample">
                                        <i class="fa fa-gear"></i> <?= $controllerName ?> Controller
                                        <span class="pull-right">
                                            <i class="showActions fa fa-plus-square fa-lg"
                                               data-name="App<?= $controllerName; ?>"></i>
                                        </span>
                                    </a>
                                </div>
                            </h4>

                            <div id="collapseApp<?= $controllerName; ?>" class="box-body collapse">
                                <table name="table_App<?= $controllerName; ?>" class="table table-hover">
                                    <tbody>
                                    <tr>
                                        <th style="width:20%">Action</th>
                                        <?php foreach ($menuGroups as $menuGroup): ?>
                                            <th style="text-align:center"><?= $menuGroup->name ?></th>
                                        <?php endforeach; ?>
                                    </tr>
                                    <?php
                                    foreach ($controller['actions'] as $action):
                                        ?>
                                        <tr>
                                            <?php if (in_array("controllers" . DS . $controllerName . DS . $action, $acosPath)): ?>
                                                <td>
                                                    <a href="#" class="description" data-toggle="popover"
                                                       data-attr='{"controller":"<?= $controllerName ?>Controller","action":"<?= $action ?>"}'>
                                                            <span class="text-warning">
                                                                <i class="fa fa-info-circle"></i>
                                                            </span>
                                                        <?= $action ?>
                                                    </a>
                                                </td>
                                                <?php
                                                foreach ($menuGroups as $menuGroup):
                                                    $json = "[['$menuGroup->name'],['controllers'],['$controllerName'],['$action'],['*']]";
                                                    ?>
                                                    <td align="center">
                                                        <a class="permission" name="<?= $json ?>" href="#">
                                                            <input type="hidden" id="<?= $json ?>" value="load"/>
                                                            <i style="color: orange;font-size: large "
                                                               class="fa  fa-spinner fa-spin"></i>
                                                        </a>
                                                    </td>
                                                <?php endforeach; ?>
                                            <?php else: ?>
                                                <td><span style="color:red; text-decoration:line-through;"><span
                                                            style="color: black"><?= $action ?></span></span></td>
                                                <?php
                                                foreach ($menuGroups as $menuGroup):
                                                    $json = "[['$menuGroup->name'],['controllers'],['$controllerName'],['$action'],['*']]";
                                                    ?>
                                                    <td align="center">
                                                        <a href="#" data-toggle="tooltip"
                                                           data-original-title="Action must be synchronized">
                                                            <input type="hidden" id="<?= $json ?>"
                                                                   value="notSynchronize"/>
                                                            <i style="color: #FFC600;font-size: large "
                                                               class="fa  fa-warning"></i>
                                                        </a>
                                                    </td>
                                                <?php endforeach; ?>
                                            <?php endif; ?>
                                        </tr>
                                    <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>

                        </div>

                    <?php endforeach; ?>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
    </div>
<?php endif; ?>

<?php if ($app['plugins']): ?>
    <div class="row">
        <div class="col-md-12">
            <div class="box box-solid box-default">
                <div class="box-header">
                    <h3 class="box-title"><i class="fa fa-plug"></i> Plugins Permission</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <?php foreach ($app['plugins'] as $pluginName => $controllers): ?>
                        <div style="margin-bottom: 2px;" class="box box-solid box-default">

                            <h4 class="box-header">
                                <div class="row">
                                    <a class="text-primary col-md-12 col-xs-11" data-toggle="collapse"
                                       href="#collapsePlugin<?= $pluginName; ?>" aria-expanded="true"
                                       aria-controls="collapseExample">
                                        <i class="fa fa-plug"></i> <?= $pluginName ?> Plugin
                                        <span class="pull-right"><i class="fa fa-plus-square fa-lg"></i></span>
                                    </a>
                                </div>
                            </h4>
                            <div id="collapsePlugin<?= $pluginName; ?>" class="box-body collapse">

                                <?php foreach ($controllers['controllers'] as $controllerName => $controller): ?>

                                    <div style="margin: 2px;" class="box box-solid box-info collapsed-box">

                                        <h4 class="box-header">
                                            <div class="row">
                                                <a class="text-primary col-md-12 col-xs-11 callShowAction"
                                                   data-toggle="collapse"
                                                   href="#collapseControllerPlugin<?= $controllerName; ?>"
                                                   aria-expanded="true" aria-controls="collapseExample">
                                                    <i class="fa fa-gear"></i> <?= $controllerName ?> Controller
                                                    <span class="pull-right">
                                                        <i class="fa fa-plus-square showActions"
                                                           data-name="ControllerPlugin<?= $controllerName; ?>"></i>
                                                    </span>
                                                </a>
                                            </div>
                                        </h4>
                                        <div id="collapseControllerPlugin<?= $controllerName; ?>"
                                             class="box-body collapse">
                                            <div class="box-body table-responsive no-padding">
                                                <table name="table_ControllerPlugin<?= $controllerName; ?>"
                                                       class="table table-hover">
                                                    <tbody>
                                                    <tr>
                                                        <th style="width:20%">Action</th>
                                                        <?php foreach ($menuGroups as $menuGroup): ?>
                                                            <th style="text-align:center"><?= $menuGroup->name ?></th>
                                                        <?php endforeach; ?>
                                                    </tr>
                                                    <?php foreach ($controller['actions'] as $action): ?>
                                                        <tr>
                                                            <?php if (in_array("controllers" . DS . $pluginName . DS . $controllerName . DS . $action, $acosPath)): ?>

                                                                <td>
                                                                    <a href="#" class="description"
                                                                       data-toggle="popover"
                                                                       data-attr='{"controller":"<?= $controllerName ?>Controller","action":"<?= $action ?>", "plugin" : "<?= $pluginName ?>"}'>
                                                                            <span class="text-warning">
                                                                                <i class="fa fa-info-circle"></i>
                                                                            </span>
                                                                        <?= $action ?>
                                                                    </a>
                                                                </td>
                                                                <?php
                                                                foreach ($menuGroups as $menuGroup):
                                                                    $json = "[['$menuGroup->name'],['$pluginName'],['$controllerName'],['$action'],['*']]";
                                                                    ?>
                                                                    <td align="center">

                                                                        <a class="permission" name="<?= $json ?>"
                                                                           href="#">
                                                                            <input type="hidden" id="<?= $json ?>"
                                                                                   value="load"/>
                                                                            <i style="color: orange;font-size: large "
                                                                               class="fa  fa-spinner fa-spin"></i>
                                                                        </a>
                                                                    </td>
                                                                <?php endforeach; ?>
                                                            <?php else: ?>
                                                                <td><span
                                                                        style="color:red; text-decoration:line-through;"><span
                                                                            style="color: black"><?= $action ?></span></span>
                                                                </td>
                                                                <?php
                                                                foreach ($menuGroups as $menuGroup):
                                                                    $json = "[['$menuGroup->name'],['$pluginName'],['$controllerName'],['$action'],['*']]";
                                                                    ?>
                                                                    <td align="center">
                                                                        <a href="#" data-toggle="tooltip"
                                                                           data-original-title="Action must be synchronized">
                                                                            <input type="hidden" id="<?= $json ?>"
                                                                                   value="notSynchronize"/>
                                                                            <i style="color: #FFC600;font-size: large "
                                                                               class="fa  fa-warning"></i>
                                                                        </a>
                                                                    </td>
                                                                <?php endforeach; ?>
                                                            <?php endif; ?>
                                                        </tr>

                                                    <?php endforeach; ?>

                                                    </tbody>
                                                </table>
                                            </div><!-- /.box-body -->
                                        </div><!-- /.box-body -->
                                    </div><!-- /.box -->


                                <?php endforeach; ?>


                            </div><!-- /.box-body -->
                        </div><!-- /.box -->
                    <?php endforeach; ?>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
    </div>
<?php endif; ?>
<!-- path to check permission, using into jquery ajax. -->
<input id="urlSetPermission" type="hidden"
       value="<?= $this->Url->build(["controller" => "Aclmanager", "action" => "setPermission"]); ?>"/>
<input id="urlCheckPermission" type="hidden"
       value="<?= $this->Url->build(["controller" => "Aclmanager", "action" => "checkPermission"]); ?>"/>
<input id="urlGetDescription" type="hidden"
       value="<?= $this->Url->build(["controller" => "Aclmanager", "action" => "getDescription"]); ?>"/>
