<?php
$class = 'danger';
if (!empty($params['class'])) {
    $class .= ' ' . $params['class'];
}

?>

<br/>
<div class="alert alert-<?= h($class) ?> alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span>
    </button>
    <?= h($message) ?>
</div>

<?php
return;

// VERSÃO ANTERIOR

$class = 'message';
if (!empty($params['class'])) {
    $class .= ' ' . $params['class'];
}
if (!isset($params['escape']) || $params['escape'] !== false) {
    $message = h($message);
}
?>
<div class="<?= h($class) ?>" onclick="this.classList.add('hidden');"><?= $message ?></div>
