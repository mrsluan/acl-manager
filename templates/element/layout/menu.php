<div class="menu-sidebar with-navbar" id="sidebar">
    <div class="collapse-sidebar list-group">

        <!-- BUILD MENU DASHBOARDS -->
        <?= $this->Html->link('<i class="fa fa-dashboard"></i> Dashboard', ['plugin' => 'AclManager', 'controller' => 'Dashboards', 'action' => 'index'], ['class' => 'list-group-item text-center', 'escape' => false]); ?>

        <!-- BUILD MENU USERS -->
        <?= $this->PluginAcl->link('<i class="fa fa-user fa-lg"></i> Users', ['controller' => 'Users', 'action' => 'index'], ['class' => 'list-group-item text-center', 'escape' => false]); ?>

        <!-- BUILD MENU GROUPS -->
        <?= $this->PluginAcl->link('<i class="fa fa-users fa-lg"></i> Groups', ['controller' => 'Groups', 'action' => 'index'], ['class' => 'list-group-item text-center', 'escape' => false]); ?>

        <!-- BUILD SUBMENU PANEL CONTROL AND GROUP PERMISSION -->
        <?php
        $subMenuPanel = $this->PluginAcl->link('<i class="fa fa-gear"></i> Panel Control', ['controller' => 'Aclmanager', 'action' => 'panelControl'],
            ['class' => 'list-group-item text-center', 'escape' => false]);

        $subMenuPermission = $this->PluginAcl->link('<i class="fa fa-check-square-o"></i> Group Permission', ['controller' => 'Aclmanager', 'action' => 'groupPermission'], ['class' => 'list-group-item text-center', 'escape' => false]);

        if ($subMenuPanel || $subMenuPermission):
            ?>
            <a href="#" submenu="sub1" data-submenu="sub1" class="list-group-item text-center">
                <i class="fa fa-gear fa-lg"></i> ACL Manager
            </a>
            <div id="sub1" class="subitem">
                <?= $subMenuPanel ?>
                <?= $subMenuPermission ?>
                <div class="list-group">
                </div>
            </div>
        <?php endif; ?>

        <!-- BUILD MENU HELP -->
        <?= $this->Html->link('<i class="fa fa-question-circle fa-lg"></i> Help', ['controller' => 'Helpers', 'action' => 'index'], ['class' => 'list-group-item text-center', 'escape' => false]); ?>

    </div>
</div>
