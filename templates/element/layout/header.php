<?php
/**
 * @var \App\View\AppView $this
 */
?>
<nav class="navbar navbar-admin navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <div class="toggle-sidebar-area">
                <button type="button" class="navbar-toggle sidebar-toggle" id="slide-submenu" tabindex="-1">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <p style="float: left; padding-top: 5px" class="hidden-sm hidden-xs"></p>
            <a class="navbar-brand nome-sistema" href="#">
                <span
                    class="hidden-xs">Admin: <?= isset($systemInfo['systemName']) ? $systemInfo['systemName'] : 'UNINFORMED'; ?></span>
                <span
                    class="visible-xs"><?= isset($systemInfo['systemAcronym']) ? $systemInfo['systemAcronym'] : 'UNINFORMED'; ?></span>
            </a>
        </div>
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                    <a href="" title="User Menu" class="dropdown-toggle" data-toggle="dropdown" role="button"
                       aria-expanded="false">
                        <!-- Nome do Usuário&nbsp; -->
                        <?= isset($systemInfo['userName']) ? $systemInfo['userName'] : 'UNINFORMED'; ?>&nbsp;
                        <i class="fa fa-user fa-lg"></i>
                        <i class="fa fa-angle-down"></i>
                    </a>
                    <ul class="dropdown-menu" role="menu">
                        <li>
                            <?php
                            $systemUrl = $systemInfo['urlBase']
                                ?? $this->Url->build('/', ['plugin' => false, 'fullBase' => false]);

                            echo $this->Html->link(
                                (isset($systemInfo['systemAcronym']) ? $systemInfo['systemAcronym'] : 'UNINFORMED'),
                                $systemUrl
                            ); ?>
                        </li>

                        <li>
                            <?= $this->Html->link('Log Off', ['plugin' => false, 'controller' => 'Users', 'action' => 'logout', '_full' => false]); ?>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>
