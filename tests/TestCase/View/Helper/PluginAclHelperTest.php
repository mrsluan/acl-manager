<?php
namespace AclManager\Test\TestCase\View\Helper;

use AclManager\View\Helper\PluginAclHelper;
use Cake\TestSuite\TestCase;
use Cake\View\View;

/**
 * AclManager\View\Helper\PluginAclHelper Test Case
 */
class PluginAclHelperTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \AclManager\View\Helper\PluginAclHelper
     */
    public $PluginAcl;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $view = new View();
        $this->PluginAcl = new PluginAclHelper($view);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->PluginAcl);

        parent::tearDown();
    }

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
