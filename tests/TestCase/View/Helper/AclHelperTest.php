<?php
namespace AclManager\Test\TestCase\View\Helper;

use AclManager\View\Helper\AclHelper;
use Cake\TestSuite\TestCase;
use Cake\View\View;

/**
 * AclManager\View\Helper\AclHelper Test Case
 */
class AclHelperTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \AclManager\View\Helper\AclHelper
     */
    public $Acl;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $view = new View();
        $this->Acl = new AclHelper($view);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Acl);

        parent::tearDown();
    }

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
