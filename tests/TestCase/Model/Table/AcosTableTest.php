<?php
declare(strict_types=1);

namespace AclManager\Test\TestCase\Model\Table;

use AclManager\Model\Table\AcosTable;
use Cake\TestSuite\TestCase;

/**
 * AclManager\Model\Table\AcosTable Test Case
 */
class AcosTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \AclManager\Model\Table\AcosTable
     */
    protected $Acos;

    /**
     * Fixtures
     *
     * @var array<string>
     */
    protected $fixtures = [
        'plugin.AclManager.Acos',
        'plugin.AclManager.Aros',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('Acos') ? [] : ['className' => AcosTable::class];
        $this->Acos = $this->getTableLocator()->get('Acos', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    protected function tearDown(): void
    {
        unset($this->Acos);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     * @uses \AclManager\Model\Table\AcosTable::validationDefault()
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     * @uses \AclManager\Model\Table\AcosTable::buildRules()
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
