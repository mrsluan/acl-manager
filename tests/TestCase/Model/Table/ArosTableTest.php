<?php
namespace AclManager\Test\TestCase\Model\Table;

use AclManager\Model\Table\GroupsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * AclManager\Model\Table\ArosTable Test Case
 */
class ArosTableTest_ extends TestCase
{

    /**
     * Test subject
     *
     * @var \AclManager\Model\Table\GroupsTable
     */
    public $Aros;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.acl_manager.aros',
        'plugin.acl_manager.acos',
        'plugin.acl_manager.aros_acos'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Aros') ? [] : ['className' => 'AclManager\Model\Table\GroupsTable'];
        $this->Aros = TableRegistry::get('Aros', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Aros);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
