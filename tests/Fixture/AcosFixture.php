<?php
declare(strict_types=1);

namespace AclManager\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * AcosFixture
 */
class AcosFixture extends TestFixture
{
    /**
     * Table name
     *
     * @var string
     */
    public $table = 'acos';
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'id' => 1,
                'parent_id' => 1,
                'model' => 'Lorem ipsum dolor sit amet',
                'foreign_key' => 1,
                'alias' => 'Lorem ipsum dolor sit amet',
                'lft' => 1,
                'rght' => 1,
            ],
        ];
        parent::init();
    }
}
