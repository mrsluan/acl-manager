<?php

namespace AclManager\View\Helper;

use Acl\Controller\Component\AclComponent;
use Cake\Controller\ComponentRegistry;
use Cake\Core\Plugin;
use Cake\Http\ServerRequest;
use Cake\Routing\Router;
use Cake\View\Helper;
use Cake\View\View;

//use Acl\Auth\ActionsAuthorize;


/**
 * PluginAcl helper
 */
class PluginAclHelper extends Helper
{

    /**
     * Helpers used.
     *
     * @var array
     */
    public $helpers = ['Html', 'Form', 'Url'];

    /**
     * Acl Instance.
     *
     * @var object
     */
    public $Acl;

    /**
     * ActionsAuthorize Instance.
     *
     * @var object
     */
    // public $Authorize;

    /**
     * Construct method.
     *
     * @param \Cake\View\View $view The view that was fired.
     * @param array $config The config passed to the class.
     */
    public function __construct(View $view, $config = [])
    {
        parent::__construct($view, $config);

        // IF NOT LOADED PLUGIN ACL RETURN.
        if (!Plugin::isLoaded('Acl')) {
            $this->Acl = NULL;
            return;
        }

        // REGISTRING COMPONENT ACL .
        $collection = new ComponentRegistry();
        $this->Acl = new AclComponent($collection);

    }

    /**
     * Check if the user can access to the given URL.
     *
     * @param array $params The params to check.
     *
     * @return bool
     */
    public function check($url = null)
    {

        // USER AND PROFILE OF THE USER LOGGED INTO SYSTEM.
        $session = $this->getView()->getRequest()->getSession();
        $mainSystem = $session->read('mainSystemInfo');

        if (empty($url)) {
            return true;
        }

        if (empty($mainSystem)) {
            return false;
        }

        // FAILURE IN LOAD AclComponent
        if (!$this->Acl) {
            return false;
        }

        if ($url !== null) {
            $url = $this->Url->build($url);
        }

        $url = Router::parseRequest(new ServerRequest(['url' => Router::normalize($url)]));

        $plugin = isset($url['plugin']) ? $url['plugin'] : '';
        $controller = isset($url['controller']) ? $url['controller'] : '';
        $action = isset($url['action']) ? $url['action'] : '';


        $profiles = isset($mainSystem['userProfile']) ? $mainSystem['userProfile'] : [];

        foreach ($profiles as $profile) {
            // OBS. THE PLUGIN ACL USE THE SLASH "/".
            if (@$this->Acl->check($profile, "$plugin/$controller/$action"))
                return true;
        }

        return false;
    }

    /**
     * Generate the link only if the user has access to the given url.
     *
     * @param string $title The content to be wrapped by <a> tags.
     * @param string|array|null $url Cake-relative URL or array of URL parameters, or
     * external URL (starts with http://)
     * @param array $options Array of options and HTML attributes.
     *
     * @return string
     */
    public function link($title, $url = null, array $options = [])
    {

        // return $this->Html->link($title, $url, $options);
        if (!$this->check($url)) {
            return '';
        }
        return $this->Html->link($title, $url, $options);
    }


    public function postLink($title, $url = null, $options = [])
    {

        if (!$this->check($url)) {
            return '';
        }

        return $this->Form->postLink($title, $url, $options);
    }


}
