<?php

namespace AclManager\View\Helper;

use Cake\View\Helper;
use Cake\View\StringTemplateTrait;
use Cake\View\View;

class PluginPageHeaderHelper extends Helper
{

    use StringTemplateTrait;

    public $helpers = ['Html'];

    protected $_title = ['label' => '$this->PluginPageHeader->addTitle( array(\'label\' => \'Title\', \'icon\'=>\'fa-\') )', 'icon' => '', 'options' => ['class' => 'col-sm-6']];

    protected $_subtitle = ['label' => '$this->PluginPageHeader->addSubTitle( array(\'label\' => \'Title\',) )', 'options' => []];

    protected $_helper = ['label' => '', 'icon' => '', 'options' => []];

    protected $_defaultConfig = [
        'templates' => [
            'title' => '<h1 {{attrs}}>{{content}}</h1>',
            'subTitle' => '<small {{attrs}}> {{content}}</small>'
        ]
    ];

    /**
     * Construct method.
     *
     * @param \Cake\View\View $view The view that was fired.
     * @param array $config The config passed to the class.
     */
    public function __construct(View $view, $config = [])
    {
        parent::__construct($view, $config);
    }

    public function addTitle($title)
    {
        $this->_title = array_merge($this->_title, $title);

    }

    public function addSubTitle($title)
    {
        // $this->_subtitle['label'] = $title['label'];
        $this->_subtitle = array_merge($this->_subtitle, $title);
    }

    public function pageHeader()
    {


        $iconTitle = isset($this->_title['icon']) ? '<i class="' . $this->_title['icon'] . '"></i>' : '';

        $templater = $this->templater();


        $subTitle = $templater->format('subTitle', [
            'attrs' => isset($this->_subtitle['options']) ? $templater->formatAttributes($this->_subtitle['options']) : '',
            'content' => $this->_subtitle['label']
        ]);

        $title = $templater->format('title', [
            'attrs' => isset($this->_title['options']) ? $templater->formatAttributes($this->_title['options']) : '',
            'content' => $iconTitle . ' ' . $this->_title['label'] . $subTitle
        ]);

        return $title;

    }


}
