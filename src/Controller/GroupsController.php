<?php

namespace AclManager\Controller;

use Cake\Core\Configure;
use Cake\Datasource\ConnectionManager;
use Cake\Event\EventInterface;
use Cake\ORM\TableRegistry;

/**
 * Groups Controller
 *
 * @property \AclManager\Model\Table\GroupsTable $Groups
 */
class GroupsController extends AppController
{

    /**
     * beforeRender method
     *
     * @return void
     */
    public function beforeRender(EventInterface $event): void
    {
        parent::beforeRender($event);
    }

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $search = !empty($this->request->getQuery('search'))
            ? $this->request->getQuery('search')
            : null;

        $conditions = $this->search($search);

        // WHEREAS ALL REGISTER WITH PARENT_ID NULL AND A GROUP
        $this->set('groups',
            $this->paginate(
                $this->Groups->find('all')
                    ->where(['parent_id IS NULL', $conditions]))
        );

        $this->set('search', $search);
    }

    /**
     * Metodo para formatar a consulta.
     */
    private function search($search)
    {
        if (!$search) {
            return null;
        }

        // NOME DO USUARIO
        $result['alias LIKE'] = "%" . mb_strtoupper($search['name']) . "%";

        return $result;
    }

    /**
     * View method
     *
     * @param string|null $id Group id.
     * @return void
     * @throws \Cake\Http\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $group = $this->Groups->get($id);

        $this->set('group', $group);
        $this->viewBuilder()->setOption('serialize', ['group']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $group = $this->Groups->newEmptyEntity();

        if ($this->request->is('post')) {

            $data = $this->request->getData();
            $tableName = $data['model'];
            $foreingKey = $data['foreign_key'];

            // IF VALIDATE...
            if ($this->validate($data, $tableName, $foreingKey)) {

                $group = $this->Groups->patchEntity($group, $this->request->getData());

                $group['alias'] = strtoupper(trim($group['alias']));

                if ($this->Groups->save($group)) {
                    $this->Flash->success(__('The group {0} has been saved.', $group->alias));
                    return $this->redirect(['action' => 'index']);
                } else {
                    $this->Flash->error(__('The group {0} could not be saved. Please, try again.', $group->alias));
                }
            }
        }

        $this->set(compact('group'));
        $this->viewBuilder()->setOption('serialize', ['group']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Group id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Http\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $group = $this->Groups->get($id);

        if ($this->request->is(['patch', 'post', 'put'])) {

            $data = $this->request->getData();

            $tableName = $data['model'];

            $foreingKey = $data['foreign_key'];

            // IF VALIDATE...
            if ($this->validate($data, $tableName, $foreingKey)) {

                $group = $this->Groups->patchEntity($group, $data);
                $group['alias'] = strtoupper(trim($group['alias']));

                if ($this->Groups->save($group)) {
                    $this->Flash->success(__('The group {0} has been saved.', $group->alias));
                    return $this->redirect(['action' => 'index']);
                } else {
                    $this->Flash->error(__('The group {0} could not be saved. Please, try again.', $group->alias));
                }
            }
        }
        $this->set(compact('group'));
        $this->viewBuilder()->setOption('serialize', ['group']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Group id.
     * @return void Redirects to index.
     * @throws \Cake\Http\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $group = $this->Groups->get($id);

        if ($this->Groups->delete($group)) {
            $this->Flash->success(__('The group {0} has been deleted.', $group->alias));
        } else {
            $this->Flash->error(__('The group {0} could not be deleted. Please, try again.', $group->alias));
        }
        return $this->redirect(['action' => 'index']);
    }

    /**
     * Validate method
     *
     * @data request post send form.
     * @tableName name of the table use with GROUP TABLE.
     * @return TRUE OR FALSE
     */
    private function validate($data, $tableName, $foreingKey)
    {

        if (trim($data['alias']) == '') {
            $this->Flash->error(__('Please inform a name for group (Alias).'));
            return FALSE;
        }

        // IF NOT SET MODEL AND FOREIGN_KEY RETURN TRUE
        if ((!isset($data['model']) || empty($data['model'])) &&
            (!isset($data['foreign_key']) || empty($data['foreign_key']))) {

            return TRUE;
        }

        // IF SET MODEL BUT NOT SET FOREIGN KEY RETURN FALSE
        if ((isset($data['model']) && !empty($data['model'])) &&
            (!isset($data['foreign_key']) || empty($data['foreign_key']))) {

            $this->Flash->error(__('It is necessary inform FOREIGN KEY. Please, try again.'));
            return FALSE;
        }

        // IF SET FOREIGN KEY BUT MODEL NOT SET  RETURN FALSE
        if ((!isset($data['model']) || empty($data['model'])) &&
            (isset($data['foreign_key']) && !empty($data['foreign_key']))) {

            $this->Flash->error(__('It is necessary inform MODEL. Please, try again.'));
            return FALSE;
        }

        // GET CONNECTION.
        $connection = Configure::read('Acl.database');

        // GET DATABASE
        $db = ConnectionManager::get($connection);

        // GET TABLES' NAME.
        $collection = $db->getSchemaCollection();
        $tables = $collection->listTables();

        if (!in_array($tableName, $tables)) {
            $this->Flash->error(__("I'm sorry not found table {0} into schema {1}. Please check your database and/or Acl config database (Acl.database).", $tableName, $connection));
            return FALSE;
        }

        // REGISTER TABLE $TableName
        if (TableRegistry::getTableLocator()->exists($tableName)) {
            $table = TableRegistry::getTableLocator()->get($tableName);
        } else {
            $table = TableRegistry::getTableLocator()->get($tableName, [
                'connection' => ConnectionManager::get($connection)
            ]);
        }

        // CHECK IF TABLE HAVE PRIMARY KEY
        $primaryKey = $table->getPrimaryKey();

        if (empty($primaryKey)) {
            $this->Flash->error(__("It is necessary define primary key in your table {0}. Please check your database and/or Acl config database (Acl.database).", $tableName));
            return FALSE;
        }

        // CHECK IF TABLENAME/FOREIGN KEY EXIST .
        $result = $table->find('all')->where([$primaryKey => $foreingKey])->count();
        if ($result <= 0) {
            $this->Flash->error(__("Not exist none register into table {0} with {1} which value is {2}. Please check your table/registers", $tableName, $primaryKey, $foreingKey));
            return FALSE;
        }
        return TRUE;
    }
}
