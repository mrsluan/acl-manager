<?php

namespace AclManager\Controller;

use Cake\Core\Plugin;
use Cake\Event\EventInterface;

/**
 * Users Controller
 *
 */
class HelpersController extends AppController
{

    private $arrayHelp = [];

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * @return void
     */
    public function initialize(): void
    {
        parent::initialize();

        if (Plugin::isLoaded('Acl') && !$this->Acl) {
            $this->allow('index');
            $this->allow('getHelper');
        }

        //$this->arrayHelp['PAGES']['DISPLAY'] = '<ul> <li> No quadro <div style="display:inline-block" class="panel panel-info"> <div class="panel-heading"> <h3 class="panel-title"><i class="fa fa-info-circle" aria-hidden="true"></i> Informações</h3> </div>  </div>, &eacute; poss&iacute;vel consultar algumas informa&ccedil;&otilde;es sobre o sistema;</li> <li> No quadro <div style="display:inline-block" class="panel panel-danger"> <div class="panel-heading"> <h3 class="panel-title"><i class="fa fa-envelope" aria-hidden="true"></i> Ultimas Mensagens</h3> </div>  </div>, &eacute; poss&iacute;vel acompanhar as mensagens enviadas e recebidas, bem como visualiz&aacute;-las. Basta clicar na mensagem desejada para visualiz&aacute;-la;</li> <li> No quadro <div style="display:inline-block" class="panel panel-warning"> <div class="panel-heading"> <h3 class="panel-title"><i class="fa fa-bell" aria-hidden="true"></i> Notificações</h3> </div>  </div>, &eacute; poss&iacute;vel acompanhar as &uacute;ltimas altera&ccedil;&otilde;es realizadas nos instrumentos jur&iacute;dicos e consultar informa&ccedil;&otilde;es dos vencimentos de documentos (dependendo da sua permiss&atilde;o).</li> </ul>';

        $this->arrayHelp['DASHBOARDS']['INDEX'] = "<h4>Welcome Acl Manager</h4>In this page you'll find the tips to configuration the Plugin Acl Manager and Plugin Acl.
                <span>Before explain functions observe than the System Acl Manager to bring side bar menu with the functionalites of the system,
                and the top header of the page you'll see headline \"Admin:\" plus your system's name and right corner you'll see user's name
                together sub-menu with your system's name and menu log off, see the picture below.  </span><br />
                <div align=\"center\"> <img src=\"../acl_manager/img/dashboard.jpg\" alt=\"dashboard\" align=\"middle\"> </div>
                Here you going see a short cut menu for main system's functions and messages, warning, erro's message.
                Showing you if the <i><b>Plugin Acl Manager</b></i> and the <i><b>Plugin Acl</b></i> is <font style=\"color:green\"><b>working</b></font> or has any <font style=\"color:red\"><b>problem</b></font>.

                <b>Do you want know more?</b>   Acess menu Help for more information.

                <i>We hope that plugin help you,</i><br /><b>Regards, Team Developers</b>. ";

        $this->arrayHelp['USERS']['INDEX'] = "<h4>List of the \"Users System\"</h4>
                 First we have do a considerations, in truth the <i>Plugin Acl</i> don't use one exclusive table for save users of the your system,
                 the <i>Plugin Acl</i> save yours users in table <b>Aros</b> and we're considered that all register of the table <b>Aros</b> whose field <i><b>parent_id</b></i> <b>IS NOT NULL</b> is a users register.
                 After of the considers can see in this page a area for <i class=\"fa fa-filter\"></i>Search and a table with list of the users where to each  user you have follow actions <a class=\"btn btn-primary btn-xs\"><i class=\"fa fa-search\"></i> View</a> <a class=\"btn btn-success btn-xs\"><i class=\"fa fa-pencil\"></i> Edit</a> <a class=\"btn btn-danger btn-xs\"><i class=\"fa fa-trash-o\"></i> Delete</a>,
                 beyond this actions you can register one new user clicking a button <a class=\"btn btn-primary\">New User</a>.

                 Below is description of the each column the table:
                 <ul>
                    <li><i><b>id</b></i> - identifier of the table;</li>
                    <li><i><b>User Name</b></i> - name of the user, in table Acos is the field Alias;</li>
                    <li><i><b>Group Name</b></i> - name of the group that user is relation, the field <i>parent_id</i> in the table <b>Acos</b> receive the id of the group;</li>
                    <li><i><b>Model</b></i> - name table that you use to save your system's users, it's not necessary for <i>Plugin Acl</i>, see menu Help;</li>
                    <li><i><b>Foreign Key</b></i> - name field in table that you use to save your system's users, it's not necessary for <i>Plugin Acl</i>, see menu Help;</li>
                 </ul>";

        $this->arrayHelp['USERS']['ADD'] = "<h4>Register a \"User\"</h4>
                One more time, the <i>Plugin Acl</i> haven't a table to save a user of the your system, for register one user, the <i>Plugin Acl Manager</i> save \"user\"
                into table <b>Acos</b> with field <i><b>parent_id</b></i> <b>IS NOT NULL </b>.
                You'll see that it's possible create one table in your system where put information about the <b>Users</b> of the your system and linked with table <b>Acos</b>, for more details acess menu Help.
                Below follow explation about each field:
                <ul>
                    <li><b>User Name (Alias) - </b> one \"Alias\" to the <i>Plugin Acl</i> <b>check</b> if the user has permission to acess the system;</li>
                    <li><b>Model - </b>remeber that we say it's possible link the table <b>Acos/User</b> with your system's table user, in this field you put the name of the your table user.</li>
                    <li><b>Foreign Key - </b> key that link the register of table <b>Acos</b> the register the your table. </li>
                    <li><b>Group for user</b> in the <i>Plugin Acl Manager</i> it's possible set permission for one group and link user the group. For this you must set
                    group of the user. This is done set <i><b>id</b></i> of the group the field <i><b>parent_id</b></i>.</li>
                </ul> <b><font style=\"color:red;\">PS.</font></b> The <i>Plugin Acl Manager</i> validate if exist the table that you set in the field Model and check if exist foreign key that you set on the field
                Foreign Key, if the contidions don't satify wasn't possible save user.
            ";

        $this->arrayHelp['GROUPS']['INDEX'] = "<h4>List of the \"Groups System\"</h4>
                 First we have do a considerations, in truth the <i>Plugin Acl</i> don't use one exclusive table for save groups (profiles) of the your system,
                 the <i>Plugin Acl</i> save yours groups (profiles) in table <b>Aros</b> and we're considered that all register of the table <b>Aros</b> whose field <i><b>parent_id</b></i> is <b>NULL</b> is a group register.
                 After of the considers can see in this page a area for <i class=\"fa fa-filter\"></i>Search and a table with list of the groups where to each  group you have follow actions <a class=\"btn btn-primary btn-xs\"><i class=\"fa fa-search\"></i> View</a> <a class=\"btn btn-success btn-xs\"><i class=\"fa fa-pencil\"></i> Edit</a> <a class=\"btn btn-danger btn-xs\"><i class=\"fa fa-trash-o\"></i> Delete</a>,
                 beyond this actions you can register one new group clicking a button <a class=\"btn btn-primary\">New Group</a>.

                 Below is description of the each column the table:
                 <ul>
                    <li><i><b>id</b></i> - identifier of the table;</li>
                    <li><i><b>Group Name</b></i> - name of the group(profile), in table Acos is the field Alias;</li>
                    <li><i><b>Model</b></i> - name table that you use to save your system's group, it's not necessary for <i>Plugin Acl</i>, see menu Help;</li>
                    <li><i><b>Foreign Key</b></i> - name field in table that you use to save your system's groups, it's not necessary for <i>Plugin Acl</i>, see menu Help;</li>
                 </ul>";

        $this->arrayHelp['GROUPS']['ADD'] = "<h4>Register a \"Group\"</h4>
                One more time, the <i>Plugin Acl</i> haven't a table to save a group of the your system, for register one group, the <i>Plugin Acl Manager</i> save \"group\" into table <b>Acos</b> with field <i><b>parent_id</b></i> <b> NULL </b>.
                You'll see that it's possible create one table in your system where put information about the <b>Users</b> of the your system and linked with table <b>Acos</b>, for more details acess menu Help.
                Below follow explation about each field:
                <ul>
                    <li><b>Group Name (Alias) - </b> one \"Alias\" to the <i>Plugin Acl</i> <b>check</b> if the user has permission to acess the system;</li>
                    <li><b>Model - </b>remeber that we say it's possible link the table <b>Acos/User</b> with your system's table user, in this field you put the name of the your table user.</li>
                    <li><b>Foreign Key - </b> key that link the register of table <b>Acos</b> the register the your table. </li>
                    <li><b>Group for user</b> in the <i>Plugin Acl Manager</i> it's possible set permission for one group and link user the group. For this you must set group of the user. </li>
                </ul> <b><font style=\"color:red;\">PS.</font></b> The <i>Plugin Acl Manager</i> validate if exist the table that you set in the field Model and check if exist foreign key that you set on the field
                Foreign Key, if the contidions don't satify wasn't possible save user.
            ";

        //$this->arrayHelp['DASHBOARDS']['INDEX'] = '<div style="text-align: left;"><strong>Welcome Acl Manager, this page help you the configuration the Plugin Acl Manager and Plugin Acl.</strong></div><div style="text-align: left;">&nbsp;</div><div style="text-align: left;">&nbsp;</div><div style="text-align: left;">After explain the functionaties this page like than look of the side menu and the page header,</div><div style="text-align: left;">all the time it&#39;s possible to see a menu bar with functions of the system, see image below, in the page header it&#39;s possible to see</div><div style="text-align: left;">your system&#39;s name and side left the name of the user logged into systme and sub-menu for go back to your system or do loggout.</div><div style="text-align: left;">&nbsp;</div>';
    }

    public function beforeRender(EventInterface $event)
    {
        parent::beforeRender($event);
    }

    /**
     * Index method
     *
     * @return void
     * @return void
     */
    public function index()
    {

    }

    public function getHelper($controller = null, $action = null)
    {

        // METODO USADO VIA AJAX.
        $this->viewBuilder()->setLayout('ajax');
        $this->disableAutoRender();

        $controller = strtoupper($controller);
        $action = strtoupper($action);

        $help = "<h4> Hasn't had help content for this page, I'm sorry :( .</h4>";

        if ($controller && $action) {
            $help = isset($this->arrayHelp[$controller][$action]) ? $this->arrayHelp[$controller][$action] : $help;
        } else if ($controller) {
            $help = isset($this->arrayHelp[$controller]) ? $this->arrayHelp[$controller] : $help;
        }

        return $this->response
            ->withType('json')
            ->withStringBody($help);

    }

}
