<?php
declare(strict_types=1);

namespace AclManager\Controller;

use App\Controller\AppController as BaseController;
use Cake\Core\Plugin;
use Cake\Event\EventInterface;

class AppController extends BaseController
{
    // HERE IS THE HELPERS USED FOR PLUGIN ACLMANAGER.
    //  public $helpers = ['AclManager.PluginAcl', 'AclManager.PluginPageHeader'];


    public function initialize(): void
    {
        parent::initialize();

        if (Plugin::isLoaded('Acl') && empty($this->Acl)) {
            $this->loadComponent('Acl.Acl');
        }

        if (!$this->isAuthorized()) {
            $this->Flash->error(__('You are not authorized to access that location.'));
            $this->redirect(['controller' => 'Dashboards', 'action' => 'index']);
            //return;
        }

    }

    public function isAuthorized()
    {

        $pl = ($this->request->getParam('plugin'));
        $co = ($this->request->getParam('controller'));
        $ac = ($this->request->getParam('action'));

        // SET VARIABLES...
        $plugin = isset($pl) ? $pl : NULL;
        $controller = isset($co) ? $co : NULL;
        $action = isset($ac) ? $ac : NULL;

        // SET VARIABLE IF PLUGIN ACL LOADED.
        $aclPluginLoaded = Plugin::isLoaded('Acl');

        // WHITHOUT CONTROLLER AND ACTION RETURN TRUE;
        if (!$controller && !$action) {
            return true;
        }

        // ALL USER CAN ACCESS DASHBOARD OF THE AclManager.
        if ($controller === 'Dashboards' && $action === 'index') {
            return true;
        }

        // ALL USER CAN ACCESS HELPERS OF THE AclManager.
        if ($controller === 'Helpers' && ($action === 'index' || $action == 'getHelper')) {
            return true;
        }

        // INFO ABOUT MAIN SYSTEM
        $session = $this->request->getSession();
        $mainSystem = $session->read('mainSystemInfo');

        // AUTHENTING THE USER ...
        if (empty($mainSystem) || !isset($mainSystem['userProfile'])) {

            $this->Flash->error(__('You have set session variable <b>mainSystemInfo</b>, see mainSystem parameters.'));

            return false;
        }


        // IF NOT LOADED PLUGIN ACL RETURN
        if (!$aclPluginLoaded) {

            $this->Flash->error(__('You have install Plugin Acl to access Plugin Manager Acl.'));

            return false;
        }


        // VERIFY IF USER HAS PERMISSION IN THE CONTROLLER/ACTION
        // OBS. THE PLUGIN ACL USE THE SLASH "/".
        foreach ($mainSystem['userProfile'] as $profile) {

            if (@$this->Acl->check($profile, "$plugin/$controller/$action")) {
                return true;
            }
        }

        return false;
    }

    public function beforeRender(EventInterface $event)
    {
        try {
            return parent::beforeRender($event);
        } finally {
            $this->viewBuilder()->setTheme(null);
        }
    }

}
