<?php

namespace AclManager\Controller\Component;

use Cake\Controller\Component;
use Cake\Controller\Controller;
use Cake\Core\App;
use Cake\Core\Configure;
use Cake\Core\Plugin;
/**
 * AclReflector component
 */
class AclReflectorComponent extends Component
{

    /**
     * Root node name.
     *
     * @var string
     */
    public $rootNode = 'controllers';


    /**
     * Default configuration.
     *
     * @var array
     */
    protected $_defaultConfig = [];


    /**
     * Get the namespace for a given class.
     *
     * @param string $className The class you want a namespace for.
     * @param string $pluginPath The plugin path.
     * @return string
     */
    protected function _getNamespace($className, $pluginPath = false): string
    {

        $className = str_replace(DS, '/', $className);
        $namespace = preg_replace('/(.*)Controller\//', '', $className);
        $namespace = preg_replace('/\//', '\\', $namespace);
        $namespace = preg_replace('/\.php/', '', $namespace);
        if (!$pluginPath) {
            $appNamespace = Configure::read('App.namespace');
            $namespace = '\\' . $appNamespace . '\\Controller\\' . $namespace;
        } else {
            $pluginPath = preg_replace('/\//', '\\', $pluginPath);
            $namespace = '\\' . $pluginPath . 'Controller\\' . $namespace;
        }

        return $namespace;
    }


    /**
     * Get the actions for a given class.
     *
     * @param string $className The class you want a namespace for.
     * @param string $pluginPath The plugin path.
     * @return array with actions
     */
    public function getControllerAction($className, $pluginPath = false)
    {

        if ($pluginPath) {

            $pluginPath .= '/';
        }
        // $excludes = $this->_getCallbacks($className, $pluginPath);
        $baseMethods = get_class_methods(new Controller);

        // debug($baseMethods);die;
        $namespace = $this->_getNamespace($className, $pluginPath);
        $reflectionClass = new \ReflectionClass($namespace);

        $actions = null;
        if ($reflectionClass->IsInstantiable()) {
            $methods = $reflectionClass->getMethods(\ReflectionMethod::IS_PUBLIC);

            foreach ($methods as $method) {
                $actions[] = $method->getName();
            }
        }

        $methods = array_diff($actions, $baseMethods);

        return $methods;
    }

    /**
     * Get the list of plugins in the application.
     *
     * @return array
     */
    public function getPluginList()
    {
        return Plugin::loaded();
    }

    /**
     * Get a list of controllers in the app and plugins.
     *
     * Returns an array of path => import notation.
     *
     * @param string $plugin Name of plugin to get controllers for
     * @return array
     */
    public function getControllerList($plugin = null)
    {
        // OBSERVAÇÃO RETORNA TODOS OS CONTROLLERS EXCETO O APPCONTROLLER.
         return $this->findControllers($plugin);
    }


    private function findControllers(?string $plugin = null): array
    {
        $controllers = [];
        //$path = $plugin ? Plugin::classPath($plugin) . 'Controller/' : APP . 'Controller/';
        $path = App::classPath('Controller', $plugin)[0];

        // Itera pelos arquivos de controller
        /** @var \DirectoryIterator $fileInfo */
        foreach (new \DirectoryIterator($path) as $fileInfo) {
            if (!$fileInfo->isDot() && !$fileInfo->isDir()
                && substr($fileInfo->getFilename(), -14) === 'Controller.php') {

                $className = $fileInfo->getBasename('.php');

                // Ignora o AppController e classes abstratas
                if ($className !== 'AppController' && !is_subclass_of($className, 'Cake\Controller\Controller')) {
                    //$controllers[] = $className;
                    $controllers[] = $fileInfo->getPathname();
                }
            }
        }

        return $controllers;
    }

    /**
     * Get all Plugin Names
     * @return multitype:Ambigous <>
     */
    public function getAllPluginNames()
    {
        return Plugin::loaded();
    }

    public function getAllAppPath($apps)
    {
        foreach ($apps as $app => $value) {
            $apps[$app] = ['path' => Plugin::path($app)];
        }

        return $apps;
    }

    public function getControllers($file)
    {
        // $files = scandir('../src/Controller/');

        $ignoreList = [
            '.',
            '..',
            'Component',
            'AppController.php',
        ];
        // foreach($files as $file){
        if (!in_array($file, $ignoreList)) {
            $controller = explode('.', $file)[0];
            // return str_replace('Controller', '', $controller);
            return $controller;
        }
        // }
        return null;
    }

    public function getDescription($className, $method = '', $pluginPath = false)
    {


        if ($pluginPath) {

            $pluginPath .= '/';
        }

        $namespace = $this->_getNamespace($className, $pluginPath);

        $reflectionClass = new \ReflectionClass($namespace);

        if (empty($method)) {
            // to get the Class DocBlock
            return $reflectionClass->getDocComment();
        }

        // to get the Method DocBlock
        return $reflectionClass->getMethod($method)->getDocComment();

    }


}
