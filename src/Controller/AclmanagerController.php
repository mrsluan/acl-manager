<?php

namespace AclManager\Controller;

use Acl\AclExtras;
use Cake\Event\EventInterface;

/**
 * Aclmanager Controller
 *
 */
class AclmanagerController extends AppController
{

    public function initialize(): void
    {
        parent::initialize();

        $this->loadComponent('RequestHandler');

        $this->loadComponent('AclManager.AclReflector');
        $this->loadComponent('Acl.Acl');
    }

    /**
     * beforeRender method
     *
     * @return void
     */
    public function beforeRender(EventInterface $event)
    {
        parent::beforeRender($event);
    }

    /**
     * controlPanel method
     *
     * Show Panel with options acl manager.
     */
    public function panelControl($parmas = NULL, $type = NULL)
    {

        if ($parmas) {
            $this->fixPermission($parmas, $type);
        }

        $groups = $this->getArosPath();

        // BUSCA OS CONTROLLERS E ACTIONS SALVOS NO BANCO
        // E RETRONANDO UM CAMINHO PARA O CONTROLLER/ACTION
        $acosPath = $this->getAcosPath();

        // BUSCA TODOS OS CONTROLLER E ACTIONS DO SISTEMA.
        $app = $this->getApp();

        // VARIAVEIS DE CONTROLE.
        $orphanPermission = FALSE; // EXISTE O CONTROLLER/ACTION NO BANCO, MAS NÃO EXISTE NO SISTEMA.
        $lackPermission = FALSE; // EXISTE O CONTROLLER/ACTION NO SISTEMA, MAS NÃO NO BANCO.

        // CRIANDO UM "CAMINHO" CONTROLLER/ACTION DOS
        // CONTROLLER/ACTIONS DA APP DO SISTEMA E ...
        $appPath = array("controllers");

        foreach ($app['app']['controllers'] as $controllerName => $controller) {
            $path = "controllers" . DS . $controllerName;
            array_push($appPath, $path);
            foreach ($controller['actions'] as $action) {
                $path = "controllers" . DS . $controllerName . DS . $action;
                array_push($appPath, $path);
            }
        }

        // ... DOS PLUGINS DO SISTEMA.
        foreach ($app['plugins'] as $pluginName => $controllers) {

            $path = "controllers" . DS . $pluginName;
            array_push($appPath, $path);

            foreach ($controllers['controllers'] as $controllerName => $controller) {

                $path = "controllers" . DS . $pluginName . DS . $controllerName;
                array_push($appPath, $path);

                foreach ($controller['actions'] as $action) {

                    $path = "controllers" . DS . $pluginName . DS . $controllerName . DS . $action;
                    array_push($appPath, $path);
                }
            }
        }


        // VARIAVEL PARA ITERAGIR SOBRE O MAIOR ARRAY, SEJA, DO
        // CONTROLLER/ACTIONS (acosPath) LIDO DO BANCO OU DO
        // CONTROLLER/ACTIONS (appPath) LIDO DOS ARQUIVOS DO SISTEMA.
        $maxSize = count($acosPath) > count($appPath) ? count($acosPath) : count($appPath);

        for ($i = 0; ($i < $maxSize) && (!$orphanPermission || !$lackPermission); $i++) {

            if (!$orphanPermission && $i < count($acosPath)) {

                $path = $this->getElement($acosPath, $i);
                $orphanPermission = !in_array($path, $appPath);
            }

            if (!$lackPermission && $i < count($appPath)) {
                $lackPermission = !in_array($appPath[$i], $acosPath);
            }

        }


        $this->set('orphanPermission', $orphanPermission);
        $this->set('lackPermission', $lackPermission);
        $this->set('groups', $groups);
    }

    private function getElement($array, $position)
    {

        $i = 0;
        foreach ($array as $value) {
            if ($i++ == $position)
                return $value;
        }

        return null;
    }

    public function fixPermission($param, $type = null)
    {


        $acl_extra = new AclExtras();
        $acl_extra->startup($this);

        switch ($param) {
            case 'update':
                $acl_extra->acoUpdate();
                break;
            case 'sync':
                $acl_extra->acoSync();
                break;
            case 'recover':
                $acl_extra->args[0] = $type;
                $acl_extra->recover();
                break;
        }

    }


    /**
     * groupPermission method
     *
     * Show Permissions of the group.
     */
    public function groupPermission()
    {

        $aroTable = $this->fetchModel('AclManager.Groups');

        $this->set('menuGroups', $aroTable->find('all')->select(['name' => 'alias'])->where(['parent_id IS NULL']));

        $acosPath = $this->getAcosPath();

        $app = $this->getApp();

        $this->set('app', $app);
        $this->set('acosPath', $acosPath);

    }

    /**
     * Metodo para formatar a consulta.
     */
    function search($search)
    {

        if (!$search) {
            return \NULL;
        }

        if ($search['fromCreateDate'] != '') {
            list ($dia, $mes, $ano) = preg_split('/[\/.-]/', $search['fromCreateDate']);
            $search['fromCreateDate'] = "$ano/$mes/$dia";
        }

        if ($search['toCreateDate'] != '') {
            list ($dia, $mes, $ano) = preg_split('/[\/.-]/', $search['toCreateDate']);
            $search['toCreateDate'] = "$ano/$mes/$dia";
        }

        // NOME DO USUARIO
        $result['UPPER(Users.name) LIKE'] = "%" . mb_strtoupper($search['name']) . "%";

        if ($search['fromCreateDate'] != '' && $search['toCreateDate'] != '') {
            $result['AND'] = sprintf('%s BETWEEN %s AND %s', 'created', "'" . $search['fromCreateDate'] . "'", "'" . $search['toCreateDate'] . "'");
        } else if ($search['fromCreateDate'] != '') {
            $result['AND'] = sprintf('%s >= %s', 'created', "'" . $search['fromCreateDate'] . "'");
        } else if ($search['toCreateDate'] != '') {
            $result['AND'] = sprintf('%s <= %s', 'created', "'" . $search['toCreateDate'] . "'");
        }

        return $result;
    }


    public function getArosPath()
    {

        $groupsTable = $this->fetchModel('AclManager.Groups');

        $list = $groupsTable->find('all')->where(['parent_id IS NULL'])->order('id');

        $result = [];

        foreach ($list as $categoryName) {

            $array = [
                'parent_id' => $categoryName->parent_id,
                'model' => $categoryName->model,
                'foreign_key' => $categoryName->foreign_key,
                'alias' => $categoryName->alias
            ];

            if ($categoryName->parent_id == null) {
                $result[$categoryName->id] = $categoryName->alias;
            } else {

                $result = $this->pushAfter($result, $categoryName->parent_id, $array, $categoryName->id);
                $result[$categoryName->id] = '|--' . $categoryName->alias;
            }
        }

        return $result;
    }

    private function pushAfter($array, $position, $element, $elementId)
    {

        $result = array();
        foreach ($array as $id => $value) {

            $result[$id] = $value;

            if ($position == $id) {
                $result[$elementId] = $element;
            }
        }

        return $result;
    }

    public function getAcosPath()
    {

        $aco = &$this->Acl->Aco;

        // $list = $aco->find( 'treeList');
        $list = $aco->find('all')->order('id');

        $result = array();

        foreach ($list as $categoryName) {

            $result[$categoryName->id] = $categoryName->alias;

            if ($categoryName->parent_id !== null) {
                $result[$categoryName->id] = $result[$categoryName->parent_id] . DS . $categoryName->alias;
            }
        }

        return $result;
    }

    /*
     *  Method checkPermission
     *  Acl Method : check
     *  ARO     : GROUPS/USERS
     *  ACO     : APP/PLUGIN/CONTROLLERS/ACTIONS
     *  ACTION  :
     *
     */
    public function checkPermission()
    {
        // METODO USADO VIA AJAX.
        $this->disableAutoRender();

        /* @var $data type */
        $data = $this->request->getData();

        $aco = $data['aco'];
        $aro = $data['aro'];
        $action = $data['action'];

        $result['permission'] = @$this->Acl->check($aro, $aco, $action);

        $json_data = json_encode($result);
        $response = $this->response
            ->withType('json')
            ->withStringBody($json_data);

        return $response;

    }

    public function getDescription()
    {

        // METODO USADO VIA AJAX.
        $this->autoRender = false;

        /* @var $data type */
        $data = $this->request->getData();

        $description = $this->AclReflector->getDescription($data['controller'], $data['action'], $data['plugin']);

        $retorno = ['error' => 'Falha no cancelamento da notificação.',
            'message' => nl2br($description)];

        echo json_encode($retorno);

        return;


    }

    /*
     *  Method setPermission
     *  Acl Method : allow ( Grant an ARO permissions to an ACO ).
     *  Acl Method : deny ( Deny an ARO permissions to an ACO ).
     *  ARO     : GROUPS/USERS
     *  ACO     : APP/PLUGIN/CONTROLLERS/ACTIONS
     *  ACTION  :
     *
     */
    public function setPermission()
    {
        /* @var $data type */
        $data = $this->request->getData();

        $aco = $data['aco'];
        $aro = $data['aro'];
        $action = $data['action'];
        $permission = $data['permission'];

        // $aro = $this->Acl->Aro->node($data['aro']);
        // $aco = $this->Acl->Aco->node($data['aco']);
        //
        if ($permission == 'grant') {
            $this->Acl->allow($aro, $aco, $action);
            $result['permission'] = 'allowed';
        } else if ($permission == 'deny') {
            $this->Acl->deny($aro, $aco, $action);
            $result['permission'] = 'denied';
        } else {
            $result['permission'] = 'failure';
        }

        $this->set(compact('result')); // Pass $data to the view
        $this->viewBuilder()->setOption('serialize', ['result']); // Let the JsonView class know what variable to use
    }

    private function getApp()
    {

        $app = ['app' => array(), 'plugins' => array()];

        // search controllers app
        $app['app']['controllers'] = $this->AclReflector->getControllerList();

        // search plugis
        $app['plugins'] = $this->AclReflector->getPluginList();

        // search controllers plugin
        $plugins = array();

        foreach ($app['plugins'] as $plugin) {

            $plugins[$plugin] = array('controllers' => array());

            $plugins[$plugin]['controllers'] = $this->AclReflector->getControllerList($plugin);
        }

        $app['plugins'] = $plugins;

        // search app actions.
        $controlerActions = array();
        foreach ($app['app']['controllers'] as $key => $controller) {

            $array = explode(DS, $controller);
            $name = end($array);
            $name = preg_replace('/\.php/', '', $name);
            $name = preg_replace('/Controller/', '', $name);

            $controlerActions[$name] = ['controller' => $controller, 'actions' => array()];
            $controlerActions[$name]['actions'] = $this->AclReflector->getControllerAction($controller);
        }
        //$actions
        $app['app']['controllers'] = $controlerActions;

        // search plugins actions
        foreach ($app['plugins'] as $plugin => $controllers) {

            $controlerActions = array();

            foreach ($controllers['controllers'] as $key => $controller) {

                $array = explode(DS, $controller);
                $name = end($array);
                $name = preg_replace('/\.php/', '', $name);
                $name = preg_replace('/Controller/', '', $name);

                $controlerActions[$name] = ['controller' => $controller, 'actions' => array()];
                $controlerActions[$name]['actions'] = $this->AclReflector->getControllerAction($controller, $plugin);
            }

            //$actions
            $app['plugins'][$plugin]['controllers'] = $controlerActions;
        }

        return $app;

    }

}
