<?php

namespace AclManager\Controller;

use Acl\AclExtras;
use Cake\Core\Configure;
use Cake\Core\Plugin;
use Cake\Datasource\ConnectionManager;
use Cake\Event\EventInterface;

// use Cake\Database\Schema\Collection;


/**
 * Dashboards Controller
 *
 * @property \AclManager\Controller\Dashboards
 */
class DashboardsController extends AppController
{

    public function initialize(): void
    {
        parent::initialize();

        if (Plugin::isLoaded('Acl') && !$this->Acl) {
            $this->Auth->allow('index');
        }
    }

    /**
     * beforeRender method
     *
     * @return void
     */
    public function beforeRender(EventInterface $event): void
    {
        parent::beforeRender($event);
    }

    /**
     * Index method
     *
     * @return void
     */
    public function index($parameter = NULL)
    {
        // PARAMETERS TO BE SENT FOR VIEW.
        $mainSystem = NULL;
        $aclPluginLoaded = FALSE;
        $aclTables = [];

        // DATABASE NAME
        $aclDatabase = Configure::read('Acl.database');

        if ($aclDatabase) {
            // CREATE DB CONNECTION TO BD DEFAULT
            $connection = ConnectionManager::get($aclDatabase);

            // CREATE A SCHEMA COLLECTION.
            $collection = $connection->getSchemaCollection();

            // INFO ABOUT MAIN SYSTEM
            $session = $this->request->getSession();
            $mainSystem = $session->read('mainSystemInfo');

            // PLUGIN ACL LOADED.
            $aclPluginLoaded = Plugin::isLoaded('Acl');

            // Get the table names
            $aclTables = $collection->listTables();
        }

        // VARIABLES PASS TO VIEW.
        $this->set(compact('mainSystem', 'aclPluginLoaded', 'aclTables', 'aclDatabase'));

        if (isset($parameter) && trim($parameter) === 'acoSyncronize') {
            $this->acoSyncronize();
        }
    }

    public function acoSyncronize()
    {

        $acl_extra = new AclExtras();

        $acl_extra->startup($this);
        //$acl_extra->controller->Flash = $this->Flash;

        //$acl_extra->Shell = null;

        $acl_extra->acoSync();


    }


}
