<?php

$controller = $this->request->getParam('controller');
$action     = $this->request->getParam('action');

$this->PluginPageHeader->addTitle( ['label' => 'Users','icon' =>'fa fa-user' ] );
$this->PluginPageHeader->addSubTitle( ['label' => 'List Users' ] );

$this->Breadcrumbs->add('<i class="fa fa-dashboard"></i> Dashboard', ['controller' => 'Dashboards'], ['escape' => false]);
$this->Breadcrumbs->add('<i class="fa fa-bars"></i> List Users', null);
$this->Breadcrumbs->add('<i class="fa fa-question-circle"></i> Help', '#', ['onclick' => "showHelper( '{$controller}', '{$action}')", 'escape' => false]);
?>

<section class="container-fluid">
    <div class="row">
        <article class="col-md-12 col-xs-12">
            <div class="box box-info">
                <div class="box-header">
                    <div class="row">
                        <h4 class="box-title col-xs-8">
                            <i class="fa fa-filter"></i> Search
                        </h4>
                        <p class="col-xs-4 text-right h4">
                            <a class="text-primary" data-toggle="collapse" href="#collapseExample"
                               aria-expanded="true" aria-controls="collapseExample">
                                <i class="fa fa-plus-square fa-lg"></i>
                            </a>
                        </p>
                    </div>
                </div>
                <div class="box-body collapse" id="collapseExample">
                    <?=$this->Form->create(); ?>
                        <div class="row">
                            <div class="form-group col-xs-12 col-md-6">
                                <?= $this->Form->input('search.name', ['label' => 'User Name', 'style' => 'text-transform:uppercase', 'type' => 'text', 'placeholder' => 'User Name', 'class' => 'form-control', 'value' => $search['name']]); ?>
                            </div>
                            <div class="form-group col-xs-12 text-right">
                                <?= $this->Form->button('Find', ['class'=>'btn btn-success' ] ); ?>
                                <?= $this->Form->button('Clear', [ 'type' => 'reset', 'class'=>'btn btn-danger' ] ); ?>
                            </div>
                        </div>
                    <?=$this->Form->end(); ?>
                </div>
            </div>
        </article>
        <article class="col-xs-12">
            <div class="box box-primary">
                <div class="box-header">
                    <?= $this->PluginAcl->link(__('New User'), ['action' => 'add'], ['class' => 'btn btn-primary']); ?>
                    <hr>
                </div>
                <div class="box-body">
                    <div class=" table-responsive">
                        <div class=" table-responsive">
                        <table class="table table-bordered table-hover table-striped table-condensed bg-white">
                            <thead>
                                <tr>
                                    <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                                    <th scope="col"><?= $this->Paginator->sort('Name', 'User Name (Alias)') ?></th>
                                    <th scope="col">Group Name (Alias)</th>
                                    <th scope="col"><?= $this->Paginator->sort('model','Model') ?></th>
                                    <th scope="col"><?= $this->Paginator->sort('foreign_key', 'Foreign Key') ?></th>
                                    <th scope="col" class="actions"><?= __('Actions') ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $_class = 0;

                            foreach ($users as $user):
                                ?>
                                <tr class="<?= $_class++ % 2 ? 'odd' : 'even' ?>" role="row">
                                    <td><?= h($user->id); ?>&nbsp;</td>
                                    <td><?= h($user->alias); ?>&nbsp;</td>
                                    <td><?= $groups[$user->parent_id] ?>&nbsp;</td>
                                    <td><?= h($user->model); ?>&nbsp;</td>
                                    <td><?= h($user->foreign_key); ?>&nbsp;</td>
                                    <td class="actions">
                                        <?= $this->PluginAcl->link('<i class="fa fa-search"></i>', ['action' => 'view', $user->id], [ 'class' => 'btn btn-primary btn-xs', 'title' => "View", 'escape' => false]) ?>
                                        <?= $this->PluginAcl->link('<i class="fa fa-pencil"></i>', ['action' => 'edit', $user->id], [ 'class' => 'btn btn-success btn-xs', 'title' => "Edit", 'escape' => false]) ?>
                                        <?= $this->PluginAcl->postLink('<i class="fa fa-trash-o"></i>', ['action' => 'delete', $user->id], [ 'confirm' => __('Are you sure you want to delete User # {0}?', $user->alias), 'class' => 'btn btn-danger btn-xs', 'title' => "Excluir", 'escape' => false]) ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                    </div>
                    <div class="row">
                        <nav class="col-md-12 text-center">
                            <ul class="pagination">
                                <?= $this->Paginator->numbers(['first' => '<< Primeira', 'prev' => '< Anterior', 'next' => 'Próximo >', 'last' => 'Última >>']) ?>
                            </ul>
                        </nav>
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <?= $this->Paginator->counter('Página {{page}} de {{pages}} ({{current}} de {{count}} registros).'); ?>
                        </div>
                    </div>
                </div>
            </div>
        </article>
    </div>
</section>



<?php return ; ?>
<h1>
Users
<small><?= $option != 'all' ? __('Users {0}',  \Cake\Utility\Inflector::camelize($option)) : 'Users System' ?></small>
</h1>

<!-- FORM SEARCH -->
<div class="box box-solid box-default">
  <div class="box-header with-border">
    <h3 class="box-title">Filter</h3>
    <div class="box-tools pull-right">
      <button class="btn btn-box-tool" data-widget="collapse"><i class="glyphicon glyphicon-minus-sign"></i></button>
    </div><!-- /.box-tools -->
  </div><!-- /.box-header -->
  <div class="box-body">
      <?= $this->Form->create(null, ['type' => 'get', 'role'=>'form']);?>
          <div class="box-body">
              <div class="col-sm-4 col-xs-12">
                  <?= $this->Form->input('search.name', ['label' => false, 'type' => 'text', 'placeholder'=>'User Name', 'class'=>'form-control',  'value'=>$search['name']]);?>
              </div>
              <div class="col-sm-3 col-xs-12">
                  <div class="input-group">
                      <div class="input-group-addon">
                          <i class="glyphicon glyphicon-calendar"></i>
                      </div>
                      <?= $this->Form->input('search.fromCreateDate', ['label' => false, 'type'=>'text', 'data-mask'=>'', 'placeholder'=>'From Created', 'data-inputmask'=>"'alias': 'dd/mm/yyyy'", 'class'=>'form-control', 'value'=>$search['fromCreateDate']]);?>
                  </div><!-- /.input group -->
              </div>
              <div class="col-sm-3 col-xs-12">
                  <div class="input-group">
                      <div class="input-group-addon">
                          <i class="glyphicon glyphicon-calendar"></i>
                      </div>
                    <?= $this->Form->input('search.toCreateDate', ['label' => false, 'type'=>'text', 'data-mask'=>'', 'placeholder'=>'To Created', 'data-inputmask'=>"'alias': 'dd/mm/yyyy'", 'class'=>'form-control', 'value'=>$search['toCreateDate']]); ?>
                  </div><!-- /.input group -->
              </div>
          </div><!-- /.box-body -->

          <div class="box-footer">
              <div class="col-sm-6 col-md-3">
	    <?= $this->Form->button($this->Html->tag('i','', ['class'=>'glyphicon glyphicon-search']).'Pesquisar',
                ['class'=>'btn btn-block btn-default', 'type'=>'sumbit']); ?>

              </div>
              <div class="col-sm-6 col-md-3">

              <?php
                echo $this->Html->link(
                    $this->Form->button('<i class="glyphicon glyphicon-plus"></i> New User',
                                        ['class'=>'btn btn-block btn-default' , 'type'=>'button']),                                             ['controller'=>'Users', 'action'=>'add'], ['escape'=>false] );
              ?>

              </div>
              <div class="col-sm-6 col-md-3">
                  <?php
                    echo $this->Html->link(
                        $this->Form->button('<i class="glyphicon glyphicon-list"></i> List Groups',
                                            ['class'=>'btn btn-block btn-default' , 'type'=>'button']),                                             ['controller'=>'Groups'], ['escape'=>false] );
                  ?>


              </div>
              <div class="col-sm-6 col-md-3">
                  <button class="btn btn-block btn-default" type="button"><i class="glyphicon glyphicon-plus"></i> New Group</button>
              </div>
          </div>
      <!-- /form -->
      <?= $this->Form->end() ?>
    </div><!-- /.box-body -->
</div><!-- /.box -->


<!-- TABLE RESULT -->
<div class="box box-solid box-default">
                <div class="box-header">
                  <h3 class="box-title">User List</h3>
                </div><!-- /.box-header -->
                <div class="box-body">


                  <div class="dataTables_wrapper form-inline dt-bootstrap" id="example1_wrapper">
                   <div class="row">
                          <div class="col-sm-12"><table aria-describedby="example1_info" role="grid" id="example1" class="table table-bordered table-striped dataTable">
                    <thead>
                      <tr role="row">

<th aria-sort="descending" aria-label="Browser: activate to sort column ascending" style="width: 70px;" colspan="1" rowspan="1" aria-controls="example1" tabindex="0" class="sorting_desc"><?= $this->Paginator->sort('id'); ?></th>

<th aria-sort="descending" aria-label="Browser: activate to sort column ascending" style="width: 207px;" colspan="1" rowspan="1" aria-controls="example1" tabindex="0" class="sorting_desc"><?= $this->Paginator->sort('name','User Name'); ?></th>

<th aria-sort="descending" aria-label="Browser: activate to sort column ascending" style="width: 207px;" colspan="1" rowspan="1" aria-controls="example1" tabindex="0" class="sorting_desc"><?= $this->Paginator->sort('gr'.'name','Group Name'); ?></th>

<th aria-sort="descending" aria-label="Browser: activate to sort column ascending" style="width: 207px;" colspan="1" rowspan="1" aria-controls="example1" tabindex="0" class="sorting_desc"><?= $this->Paginator->sort('created'); ?></th>
<th aria-label="Rendering engine: activate to sort column ascending" style="width: 162px;" colspan="1" rowspan="1" aria-controls="example1" tabindex="0" class="sorting"><?= $this->Paginator->sort('modified'); ?></th>

<th aria-label="Rendering engine: activate to sort column ascending" style="width: 162px;" colspan="1" rowspan="1" aria-controls="example1" tabindex="0" class="actions sorting"><?= __('Actions'); ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                                $_class = 0;

                                foreach ($users as $user):
                        ?>
                                <tr class="<?= $_class++ % 2 ? 'odd' : 'even' ?>" role="row">
                                    <td><?= h($user->id); ?>&nbsp;</td>
                                    <td><?= h($user->name); ?>&nbsp;</td>
                                    <td><?= h($user->gr['name']); ?>&nbsp;</td>
                                    <td><?= h($user->created); ?>&nbsp;</td>
                                    <td><?= h($user->modified); ?>&nbsp;</td>
                                    <td class="actions">
                                        <?= $this->Html->link(__('View'), ['action' => 'view', $user->id]); ?>
                                        <?= $this->Html->link(__('Edit'), ['action' => 'edit', $user->id]); ?>
                                        <?=
                                        $this->Form->postLink(__('Delete'), ['action' => 'delete', $user->id], ['confirm' => __('Are you sure you want to delete User {0} ?', $user->name)]);
                                        ?>
                                    </td>
                                </tr>
                        <?php endforeach; ?>

                                      </tbody>
                    <tfoot>
                      <tr>
                          <th colspan="1" rowspan="1">Id</th>
                          <th colspan="1" rowspan="1">User Name</th>
                          <th colspan="1" rowspan="1">Group</th>
                          <th colspan="1" rowspan="1">Create</th>
                          <th colspan="1" rowspan="1">Modified</th>
                      </tr>
                    </tfoot>
                  </table></div></div>

                      <div class="row">
                          <div class="col-sm-5">
                              <div aria-live="polite" role="status" id="example1_info" class="dataTables_info">

                                  <?php

                                    $page = $this->Paginator->counter('{{page}}');
                                    $pages = $this->Paginator->counter('{{pages}}');
                                    echo $this->Paginator->counter('Showing {{page}} to {{pages}}, {{count}} entries');

                                  ?>

                              </div>
                          </div>
                          <div class="col-sm-7">
                              <div id="example1_paginate" class="dataTables_paginate paging_simple_numbers">
                                  <ul class="pagination">


                                      <li class="paginate_button previous disabled" id="example1_previous">
                                        <?= $this->Paginator->prev('Previous');?>
                                      </li>

                                      <li class="paginate_button">
                                      <?= $this->Paginator->numbers() ?>
                                      </li>
                                      <li class="paginate_button next" id="example1_next">
                                            <?= $this->Paginator->next('Next'); ?>
                                      </li>
                                  </ul>
                              </div>
                          </div>
                      </div>
                    </div>
                </div><!-- /.box-body -->

</div>



<!-- FIM -->

