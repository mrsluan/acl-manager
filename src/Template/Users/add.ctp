<?php

$controller = $this->request->getParam('controller');
$action     = $this->request->getParam('action');

$this->PluginPageHeader->addTitle( ['label' => 'User' , 'icon' => 'fa fa-plus'] );
$this->PluginPageHeader->addSubTitle( ['label' => 'User Register' ] );

$this->Breadcrumbs->add('<i class="fa fa-dashboard"></i> Dashboard', ['controller' => 'Dashboards'], ['escape' => false]);
$this->Breadcrumbs->add('<i class="fa fa-bars"></i> List Users', ['action' => 'index'], ['escape' => false]);
$this->Breadcrumbs->add('<i class="fa fa-plus"></i> Register User', null );
$this->Breadcrumbs->add('<i class="fa fa-question-circle"></i> Help', '#', ['onclick' => "showHelper( '{$controller}', '{$action}')", 'escape' => false]);

?>


<section class="container-fluid">
    <div class="row">
        <div class="box box-primary">
            <div class="box-header">
                <legend><?= __('User Register') ?></legend>
            </div>
            <div class="box-body">
                <?= $this->Form->create($user); ?>
                <div class="row">
                    <div class="form-group col-md-6">
                        <?php
                        echo $this->Form->input('alias', array(
                            'label'        => 'User Name (Alias)',
                            'class'        => 'form-control text-uppercase',
                            'type'         => 'text',
                            'placeholder'  => 'User Name (Alias)',
                            'class'        => 'form-control text-uppercase'
                        ));

                        ?>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-6">
                        <?php
                        echo $this->Form->input('model', array(
                            'label' => 'Model',
                            'class' => 'form-control',
                        ));
                        ?>
                    </div>
                    <div class="form-group col-md-3">
                        <?php
                        echo $this->Form->input('foreign_key', array(
                            'label' => 'Foreign Key',
                            'class' => 'form-control',
                        ));
                        ?>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group col-md-6">
                        <?php
                        echo $this->Form->input('parent_id', array(
                            'label' => 'You need choose a group for user',
                            'class' => 'form-control',
                            'type' =>'select',
                            'empty' => 'Choose one..',
                            'options' => $groups
                        ));
                        ?>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-5 col-md-offset-2">
                        <?= $this->Form->button( __( '<i class="fa fa-save"></i>  Save'),  [ 'type' =>'submit', 'class' => "btn btn-success"]); ?>
                        <?= $this->Form->button( __( '<i class="fa fa-ban"></i>  Cancel'), [ 'type' =>'reset',  'class' => "btn btn-danger"]); ?>
                    </div>
                </div>
                <?= $this->Form->end() ?>
            </div>

        </div>
    </div>
</section>
