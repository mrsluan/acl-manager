<?php
$this->PluginPageHeader->addTitle(['label' => 'Helpers', 'icon' => 'fa fa-question-circle']);
$this->PluginPageHeader->addSubTitle(['label' => 'List Helper']);

$this->Breadcrumbs->add('<i class="fa fa-dashboard"></i> Dashboard', ['controller' => 'Dashboards'], ['escape' => false]);
$this->Breadcrumbs->add('<i class="fa fa-question-circle"></i> List Helper', null);
?>

<style>
    .help-content{
        line-height: 1.6;
        color: #6c7d8e;
        font-size: 1.1rem;
        font-family: "Open Sans",sans-serif;
    }


</style>

<section class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-success">
                <div class="box-body">

                    <div class="row">

                        <div class="col-md-10 col-md-offset-1">

                            <ul id="basics" class="list-unstyled">
                                <li>
                                    <span style="color:#bbbbc7"><h3>Basics</h3></span>
                                </li>
                                <li>

                                    <button type="button" class="btn btn-info btn-block btn-lg" data-toggle="collapse" data-target="#help01">
                                        <span class="pull-left">What is System Application Control ?</span> <i class="fa fa-angle-down pull-right"></i>
                                    </button>
                                    <div  id="help01" class="collapse help-content">
                                        <p>System Application control is a system that blocks or restricts unauthorized users from executing actions not allowed. The control actions vary based on the business purpose of the specific application, but the main objective is to help ensure the security of system
                                        allowed or not allowed users access actions of the application.</p>
                                    </div>
                                </li>
                                <br />

                                <li>
                                    <button type="button" class="btn btn-info btn-block btn-lg" data-toggle="collapse" data-target="#help02">
                                        <span class="pull-left">What is necessary know to use it ? </span><i class="fa fa-angle-down pull-right"></i>
                                    </button>
                                    <div  id="help02" class="collapse help-content">
                                        <p>The Control Access of the users into System use stander called <b>"ACL"</b>, acronymos for <i>"Access Control List"</i>,
                                        the plugin check user perfil to control accesss the application.</p>
                                    </div>
                                </li>

                                <br />

                                <li>
                                    <button type="button" class="btn btn-info btn-block btn-lg" data-toggle="collapse" data-target="#help03">
                                        <span class="pull-left">Is it possible built application and after user system control ? </span><i class="fa fa-angle-down pull-right"></i>
                                    </button>
                                    <div  id="help03" class="collapse help-content">
                                        <p><b>YES!!!</b>, this is main objective of the Plugin + Acl
                                        leave programer free to develop application without worry users permissions,
                                         the finish application the develop set permission used Plugin and ACL.
                                         </p>
                                    </div>
                                </li>
                                <br />

                                <li>
                                    <button type="button" class="btn btn-info btn-block btn-lg" data-toggle="collapse" data-target="#help04">
                                        <span class="pull-left">Do I need to pay for use it? </span><i class="fa fa-angle-down pull-right"></i>
                                    </button>
                                    <div  id="help04" class="collapse help-content">
                                        <p><b>NO!!!</b>, it's free to use modify and to send suggests, <i>it's enjoy</i>.</p>
                                    </div>
                                </li>
                            </ul> <!-- cd-faq-group -->

                            <ul id="pluginAclManager" class="list-unstyled">
                                <li>
                                    <span style="color:#bbbbc7"><h3>Plugin Acl Manager</h3></span>
                                </li>
                                <li>
                                    <button type="button" class="btn btn-info btn-block btn-lg" data-toggle="collapse" data-target="#help05">
                                        <span class="pull-left">What is the Plugin Acl Manager? </span><i class="fa fa-angle-down pull-right"></i>
                                    </button>
                                    <div  id="help05" class="collapse help-content">
                                        <p>It's a application to help set ACL used graphic interface that can added in your application
                                        whith interfere into application. You can use ACL through command line therefore whith <b>Plugin Acl Manager</b>
                                        the contrary not is true. </p>
                                    </div>
                                </li>
                                <br />

                                <li>
                                    <button type="button" class="btn btn-info btn-block btn-lg" data-toggle="collapse" data-target="#help06">
                                        <span class="pull-left">How do I install Plugin Acl Manager? </span><i class="fa fa-angle-down pull-right"></i>
                                    </button>
                                    <div  id="help06" class="collapse help-content">
                                        <p>You can use composer to do download of the <b>Plugin Acl Manager</b> into your application, using the command
                                        <code>composer require mrsilva/AclManager</code>.</p>
                                    </div>
                                </li>
                                <br />

                                <li>
                                    <button type="button" class="btn btn-info btn-block btn-lg" data-toggle="collapse" data-target="#help07">
                                        <span class="pull-left">How do I use the Plugin Acl Manager? </span><i class="fa fa-angle-down pull-right"></i>
                                    </button>
                                    <div  id="help07" class="collapse help-content">
                                        <p>After download plugin and load in your application into address your web application to add
                                        <code>admin/dashboards</code> in this page the plugin system to guide that to do to use ACL and plugin manager.</p>
                                    </div>
                                </li>
                            </ul>



                            <ul id="pluginAcl" class="list-unstyled">
                                <li>
                                    <span style="color:#bbbbc7"><h3>Plugin Acl</h3></span>
                                </li>
                                <li>
                                    <button type="button" class="btn btn-info btn-block btn-lg" data-toggle="collapse" data-target="#help08">
                                        <span class="pull-left">What is the Plugin Acl? </span><i class="fa fa-angle-down pull-right"></i>
                                    </button>
                                    <div  id="help08" class="collapse help-content">
                                        <p>Stands for "Access Control List." An ACL is a list of user permissions for a file, folder, or other object.
                                        It defines what users and groups can access the object and what operations they can perform.
                                        These operations typically include read, write, and execute. For example, if an ACL specifies read-only access
                                        for a specific user of a file, that user will be able open the file, but cannot write to it or run the file.</p>
                                    </div>
                                </li>
                                <br />

                                <li>
                                    <button type="button" class="btn btn-info btn-block btn-lg" data-toggle="collapse" data-target="#help09">
                                        <span class="pull-left">How do I install Plugin Acl? </span><i class="fa fa-angle-down pull-right"></i>
                                    </button>
                                    <div  id="help09" class="collapse help-content">
                                    <p>You can use composer to do download of the <b>Plugin Acl</b> into your application, using the command
                                        <code>composer require cakephp/acl</code>.</p>
                                    </div>
                                </li>
                                <br />

                                <li>
                                    <button type="button" class="btn btn-info btn-block btn-lg" data-toggle="collapse" data-target="#help10">
                                        <span class="pull-left">How do I use the Plugin Acl? </span><i class="fa fa-angle-down pull-right"></i>
                                    </button>
                                    <div  id="help10" class="collapse help-content">
                                        <p>You start seeing page CakePhp tutorial how to use ACL .<a href="https://book.cakephp.org/1.3/en/The-Manual/Tutorials-Examples/Simple-Acl-controlled-Application.html">Simple-Acl-controlled-Application</a></p>
                                    </div>
                                </li>
                            </ul>

                        </div>
                    </div>




                </div>

                <div class="box-footer">
                    <div> <br /><br /> </div>
                </div>
            </div>
        </div>
    </div>
</section>
