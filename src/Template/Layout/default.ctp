<?php

 // SESSION VARIABLE PASSED INFO ABOUT MAIN SYSTEM
$session    = $this->request->getSession();
$mainSystem = $session->read('mainSystemInfo');


?>


<!DOCTYPE html>
<html lang="pt-br">
    <head>
<?= $this->Html->charset() ?>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>
            AclManager
            <?= isset($systemInfo['systemAcronym']) ? ' - '.$systemInfo['systemAcronym'] : ''; ?>:
            <?php //= $this->fetch('title'); ?>
        </title>
            <?= $this->Html->meta('icon') ?>

        <!-- CSS PADRÃO DO NTI E CSS DO SISTEMA -->
        <?= $this->Html->css('AclManager.admin.css') ?>
        <?= $this->Html->css('AclManager.bootstrap-dialog.min.css'); ?>

        <!-- COMPONENTES UTILIZADOS -->
        <?= $this->Html->script('AclManager.jQuery-2.1.4.min');   ?>
        <?php //= $this->Html->script('../vendor/bootstrap/js/bootstrap.min');   ?>
        <?php //= $this->Html->script('../vendor/notify/notify.min'); ?>


        <?= $this->fetch('meta') ?>
        <?= $this->fetch('css') ?>
        <?= $this->fetch('script') ?>

    </head>

    <body>
        <header>
            <?= $this->element('Layout/header', [ 'systemInfo' => $mainSystem, 'usuario' => isset( $mainSystem['userName'] ) ? $mainSystem['userName'] : 'UNINFORMED'  ]); ?>
        </header>
        <div class="wrapper">
            <div class="menu-sidebar with-navbar" id="sidebar">
                <div class="collapse-sidebar list-group">
                    <?= $this->element('Layout/menu'); ?>
                </div>
            </div>
            <section class="content">
                <div class="main-view">
                    <section class="container-fluid page-header">
                        <div class="row col-xs-12">
                            <?= $this->PluginPageHeader->pageHeader( );  ?>
                            <?= $this->Breadcrumbs->render(
                                ['class' => "col-sm-6 breadcrumb text-right lista", 'lastClass' => 'active']
                            ) ?>
                        </div>
                    </section>
                    <section class="container-fluid">
                        <div class="row col-sm-12">
                            <?= $this->Flash->render() ?>
                            <?= $this->Flash->render('auth') ?>
                        </div>
                        <div class="row col-sm-12">
                            <?php echo $this->fetch('content') ?>
                        </div>
                    </section>
                </div>
                <footer class="main-footer">&nbsp;&nbsp;
                        Acl Manager - Admin
                        <?php //= $this->element('Layout/footer', ['systemInfo' => $systemInfo]) ?>
                </footer>
            </section>
        </div>

        <?= $this->Html->scriptBlock( "urlHelper = '{$this->Url->build(['controller' => 'Helpers', 'action' => 'getHelper'])}'",['block' => false]); ?>

        <?= $this->Html->script('AclManager.vendor.js'); ?>
        <?= $this->Html->script('AclManager.admin.js'); ?>
        <?= $this->Html->script('AclManager.bootstrap-dialog.min.js'); ?>
    </body>
</html>
