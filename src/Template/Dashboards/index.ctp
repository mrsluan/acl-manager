<?php

$controller = $this->request->getParam('controller');
$action     = $this->request->getParam('action');

$this->PluginPageHeader->addTitle( ['label' => 'Dashboard','icon' =>'fa fa-dashboard' ] );
$this->PluginPageHeader->addSubTitle( ['label' => '' ] );

$this->Breadcrumbs->add('<i class="fa fa-dashboard"></i> Dashboard', null, ['escape' => false]);
$this->Breadcrumbs->add('<i class="fa fa-question-circle"></i> Help', '#', ['onclick' => "showHelper( '{$controller}', '{$action}')", 'escape' => false]);

?>

<?php


//echo $this->Html->image('AclManager.dashboard.jpg', ['alt' => 'CakePHP']);
//die;
?>

<section class="container-fluid">
    <div class="row">
        <article class="col-xs-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h3>Plugin AclManager <i>Managed System</i>: <b><?= isset( $mainSystem['systemName'] ) ? $mainSystem['systemName'] : 'UNINFORMED'  ?></b></h3>
                    <hr>


                </div>
                <div class="box-body">
                    <div class="row col-md-10">
                        <div class="alert alert-success" role="alert">
                            <b>Thank you</b> for use the Acl Manager Plugin, we hope that plugin help you manager yours
                        <?= $this->Html->image('https://cakephp.org/favicons/favicon.png'); ?> CakePHP projects and
                        Thanks for all contribuition to improve plugin.<br />
                        This page you can meet main shortcut to function of the plugin Acl Manager and tips for configuration plugin.
                        <br /><br />
                        The use of the AclManager Plugin follows <i class="fa fa-creative-commons" aria-hidden="true">Creative Commons license</i>
                        and the authors do not take responsibility for any errors that may happen.
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-offset-1 col-md-10">
						<?php


                            if( $aclPluginLoaded   &&
                                ! empty($aclDatabase) &&
                                in_array('acos', $aclTables ) &&
                                in_array('aros', $aclTables ) &&
                                in_array('aros_acos', $aclTables ) ):
                        ?>

                            <?= $this->PluginAcl->link(
                                     '<i class="fa fa-users fa-lg pull-left"></i> <span>Groups &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br> <small>List User Groups <br />of the System.</small></span>',
                                     ['controller'=>'Groups', 'action' => 'index' ],
                                     ['class'=>'btn btn-lg btn-primary', 'escape'=>false] );
                            ?>

                            <?= $this->PluginAcl->link(
                                     '<i class="fa fa-check-square-o fa-lg pull-left"></i> <span>Group Permission<br> <small>Set Permissions </br> for group.</small></span>',
                                     ['controller'=>'Aclmanager', 'action' => 'groupPermission' ],
                                     ['class'=>'btn btn-lg btn-danger', 'escape'=>false] );
                            ?>

                            <?= $this->PluginAcl->link(
                                     '<i class="fa fa-gear fa-lg pull-left"></i> <span>Panel Control<br> <small>Manager Plugin Acl,</br> update it and syncronize it.</small></span>',
                                     ['controller'=>'Aclmanager', 'action' => 'panelControl' ],
                                     ['class'=>'btn btn-lg btn-warning', 'escape'=>false] );
                            ?>
						<?php endif; ?>

                           <?= $this->Html->link(
                                     '<i class="fa fa-question-circle fa-lg pull-left"></i> <div style="text-align:left">Help</div><span> <small>Help content information about </br> Plugin Acl and Plugin Manager.</small></span>',
                                     ['controller'=>'Helpers', 'action' => 'index' ],
                                     ['class'=>'btn btn-lg btn-success', 'escape'=>false] );
                            ?>

                           <br /><br /><br />
                        </div>
                    </div>
                    <div class="row col-md-10">
                        <div class="alert alert-success" role="alert">
                            <i class="fa fa-check-circle"></i>
                            <b>Congratulations</b>, the AclManager plugin was successfully installed.
                            If nescessary the system show you the lack of configuration so that the system AclManager will work 100%.
                        </div>
                    </div>

                    <?php if( empty($aclDatabase) ): ?>
                        <div class="row col-md-10">
                            <div class="alert alert-danger" role="alert">
                                <i class="fa fa-ban"></i>
                                Configure database not found, you need config database in your application see file app.php Datasources.
                            </div>
                        </div>
                    <?php endif; ?>

                    <?php if( ! in_array('acos', $aclTables ) || ! in_array('aros', $aclTables ) || ! in_array('aros_acos', $aclTables ) ): ?>
                        <div class="row col-md-10">
                            <div class="alert alert-danger" role="alert">
                                <i class="fa fa-ban"></i>
                                You need to create the
                                <?= ! in_array('acos', $aclTables ) ? 'table Acos,' : '' ?>
                                <?= ! in_array('aros', $aclTables ) ? 'table Aros,' : '' ?>
                                <?= ! in_array('aros_acos', $aclTables ) ? 'table Aros_Acos,' : '' ?>
                                in your system to use plugin Acl.
                                <br />
                                <br />
                                You can use the command line to create the tables ACOS, AROS and AROS_ACOS for this, insert
                                the command: <code><?='bin'.DS.'cake';?> Migrations.migrations migrate -p Acl</code>
                                <br />
                                To know more information about tables AROS, ACOS and AROS_ACOS access <a href="http://book.cakephp.org/1.3/en/The-Manual/Tutorials-Examples/Simple-Acl-controlled-Application.html">Acl Controlled Application</a> .
                            </div>
                        </div>
                    <?php endif; ?>

                    <?php if( $aclPluginLoaded  ): ?>


                        <div class="row col-md-10">
                            <div class="alert alert-warning" role="alert">
                                <i class="fa fa-warning"></i>
                                <b>Certify</b> that your user have perfil administrator to set the permission to other users.
                                <br /><br />
                                You can register a perfil of the Administrator using command line,<br /><br />
                                <ul>
                                    <li>First you have sync the table ACOS with all CONTROLLERS/ACTION of the system, for this
                                        click no link the follow
                                    <?= $this->Html->link('Syncrozine System and Plugin Acl', ['plugin' => 'AclManager', 'controller' => 'Dashboards', 'action' =>'index', 'acoSyncronize']); ?>.

                                    <li>Second create a perfil of the Administrator, for this use command line<code><?='bin'.DS.'cake';?> acl create aro root ADMIN</code>.</li>

                                    <li>Third you have set perfil Administrator to access all features of the Acl Manager, for this
                                use the command line <code><?='bin'.DS.'cake';?> acl grant ADMIN controllers</code>.</li>
                                </ul>
                                <br />
                                * Not forget of the pass the perfil ADMIN to plugin <i>Acl Manager</i> through variable
                                <code>mainSystemInfo</code>.

                            </div>
                        </div>
                    <?php endif; ?>

                    <?php if( empty($mainSystem) ): ?>
                        <div class="row col-md-10">
                            <div class="alert alert-danger" role="alert">
                                <i class="fa fa-ban"></i>
                                You need set session variable mainSystemInfo with parameters, remeber delete $session variable:
                                <br /><br />
                                <ul>
                                    <li>//<b>session</b> - registering session. <br /><code>$session = $this->request->session();</code> </li>
                                    <li>//<b>delete</b> - deleting session cache. <br /><code>$session->delete('mainSystemInfo');</code> </li>
                                    <li>//<b>systemAcronym</b> - your system Acronym. <br /><code>$session->write('mainSystemInfo.systemAcronym', 'SYS...');</code> </li>
                                    <li>//<b>systemName</b> - your system Name. <br /><code>$session->write('mainSystemInfo.systemName', 'SYSTEM NAME');</code></li>
                                    <li>//<b>systemName</b> - your system Url. <br /><code>$session->write('mainSystemInfo.urlBase', 'Your System urlBase');</code></li>
                                    <li>//<b>userName</b> - name of the user  logged. <br /><code>$session->write('mainSystemInfo.userName', 'USER NAME');</code></li>
                                    <li>//<b>userProfile</b> - permission of the usr logged. <br /><code>$session->write('mainSystemInfo.userProfile', ['ADMIN', 'SUPERV']);</code></li>
                                </ul>
                            </div>
                        </div>
                    <?php endif; ?>

                    <?php if( ! $aclPluginLoaded  ): ?>
                        <div class="row col-md-10">
                            <div class="alert alert-danger" role="alert">
                                <i class="fa fa-ban"></i>
                                The Plugin ACL wasn't install or not loaded, please install Pluling ACL and
                                certify that it was loading.
                                <br /><br />
                                To know more about plugin ACL access <a href="https://github.com/cakephp/acl">Plugin Acl</a> and
                                <a href="https://book.cakephp.org/1.3/en/The-Manual/Core-Components/Access-Control-Lists.html">Access Control Lists</a> .
                            </div>
                        </div>
                    <?php else: ?>
                        <div class="row col-md-10">
                            <div class="alert alert-success" role="alert">
                                <i class="fa fa-check-circle"></i>
                                The Plugin ACL was install and loading whith success.
                                <br /><br />
                                To know more about plugin ACL access <a href="https://github.com/cakephp/acl">Plugin Acl</a> and
                                <a href="https://book.cakephp.org/1.3/en/The-Manual/Core-Components/Access-Control-Lists.html">Access Control Lists</a> .
                            </div>
                        </div>
                    <?php endif;?>


                    <div class="row">

                    </div>

                </div>
            </div>
        </article>
    </div>
</section>
