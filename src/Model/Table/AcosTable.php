<?php
declare(strict_types=1);

namespace AclManager\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Acos Model
 *
 * @property \AclManager\Model\Table\AcosTable&\Cake\ORM\Association\BelongsTo $ParentAcos
 * @property \AclManager\Model\Table\AcosTable&\Cake\ORM\Association\HasMany $ChildAcos
 * @property \AclManager\Model\Table\ArosTable&\Cake\ORM\Association\BelongsToMany $Aros
 *
 * @method \AclManager\Model\Entity\Aco newEmptyEntity()
 * @method \AclManager\Model\Entity\Aco newEntity(array $data, array $options = [])
 * @method \AclManager\Model\Entity\Aco[] newEntities(array $data, array $options = [])
 * @method \AclManager\Model\Entity\Aco get($primaryKey, $options = [])
 * @method \AclManager\Model\Entity\Aco findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \AclManager\Model\Entity\Aco patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \AclManager\Model\Entity\Aco[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \AclManager\Model\Entity\Aco|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \AclManager\Model\Entity\Aco saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \AclManager\Model\Entity\Aco[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \AclManager\Model\Entity\Aco[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \AclManager\Model\Entity\Aco[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \AclManager\Model\Entity\Aco[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TreeBehavior
 */
class AcosTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('acos');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Tree');

        $this->belongsTo('ParentAcos', [
            'className' => 'AclManager.Acos',
            'foreignKey' => 'parent_id',
        ]);
        $this->hasMany('ChildAcos', [
            'className' => 'AclManager.Acos',
            'foreignKey' => 'parent_id',
        ]);
        $this->belongsToMany('Aros', [
            'foreignKey' => 'aco_id',
            'targetForeignKey' => 'aro_id',
            'joinTable' => 'aros_acos',
            'className' => 'AclManager.Aros',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('parent_id')
            ->allowEmptyString('parent_id');

        $validator
            ->scalar('model')
            ->maxLength('model', 255)
            ->allowEmptyString('model');

        $validator
            ->integer('foreign_key')
            ->allowEmptyString('foreign_key');

        $validator
            ->scalar('alias')
            ->maxLength('alias', 255)
            ->allowEmptyString('alias');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn('parent_id', 'ParentAcos'), ['errorField' => 'parent_id']);

        return $rules;
    }
}
