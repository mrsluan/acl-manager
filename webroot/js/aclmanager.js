/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


jQuery(document).ready(function () {

    // alert('It is ready');
    const csrfToken = $('meta[name="csrfToken"]').attr('content');

    jQuery('[data-toggle="tooltip"]').tooltip({
        placement: 'top'
    });

    iconChildren = null;

    jQuery('.callShowAction').click(
        function () {

            var box = jQuery(this).parents(".box").first();
            var plusSquare = jQuery(this).children().find('.showActions');
            console.log(plusSquare);
            //return true;
            showActions(plusSquare);
                return true;
         }
    );

    jQuery('.showActions').click(
            function () {
                showActions(this);
                return true;

                var box = jQuery(this).parents(".box").first();

                var tableName = 'table_' + jQuery(this).attr('data-name');

                var collapseId = '#collapse' + jQuery(this).attr('data-name');

                // CODE TO BE EXECUTE IF COLLAPSED
                if ( jQuery(collapseId).hasClass('in') ) {

                    jQuery('table[name="' + tableName + '"]').find("td:has(a.permission)").each(
                            function () {
                                var input = jQuery(this).find('input:hidden');
                                // console.log(input);
                                if (jQuery(input).val() != 'notSynchronize') {
                                    var icon = jQuery(this).find('i');

                                    jQuery(input).val('load');
                                    jQuery(icon).css("color", "orange");
                                    jQuery(icon).attr('class', 'fa  fa-spinner fa-spin');
                                }
                            }
                    );


                    return;
                }

                tds = new Array();

                jQuery('table[name="' + tableName + '"]').find("td:has(a.permission)", tds).each(
                        function () {
                            tds.push(jQuery(this));
                        }
                );

                checkPermission(tds);
            }
    );

    function showActions(objeto){


            var box = jQuery(objeto).parents(".box").first();

            var tableName = 'table_' + jQuery(objeto).attr('data-name');

            var collapseId = '#collapse' + jQuery(objeto).attr('data-name');

            // CODE TO BE EXECUTE IF COLLAPSED
            if ( jQuery(collapseId).hasClass('in') ) {

                jQuery('table[name="' + tableName + '"]').find("td:has(a.permission)").each(
                        function () {
                            var input = jQuery(objeto).find('input:hidden');
                            // console.log(input);
                            if (jQuery(input).val() != 'notSynchronize') {
                                var icon = jQuery(objeto).find('i');

                                jQuery(input).val('load');
                                jQuery(icon).css("color", "orange");
                                jQuery(icon).attr('class', 'fa  fa-spinner fa-spin');
                            }
                        }
                );


                return;
            }

            tds = new Array();

            jQuery('table[name="' + tableName + '"]').find("td:has(a.permission)", tds).each(
                    function () {
                        tds.push(jQuery(this));
                    }
            );

            checkPermission(tds);

    }

    function checkPermission(tds) {


        if (tds.length > 0) {
            td = tds.shift();

            var objPermission = jQuery(td).find('a.permission');

            var name = jQuery(objPermission).attr('name');


            iconChildren = jQuery(objPermission).children('i');

            var _aco = '';
            var _aro = '';
            var _action = '*';

            vetor = eval(name);

            if (vetor.length > 1) {
                _aro = vetor[0][0];
                var DS = '';
                for (var i = 1; i < vetor.length - 1; i++) {
                    _aco += DS + vetor[i][0];
                    DS = '/';
                }

                _action = vetor[vetor.length - 1][0];
            }

            // Create URL dynamic
            var _url = jQuery('#urlCheckPermission').val();

            jQuery.ajax({
                type: 'post',
                url: _url,

                context: { iconChildren : iconChildren },

                beforeSend: function (xhr) {
                    xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
                },
                data: {
                    aro: _aro,
                    aco: _aco,
                    action: _action
                },
                dataType: 'json',
                headers: {
                    'X-CSRF-TOKEN': csrfToken
                },
                success: function (response) {

                     // alert(response.result);
                     console.log(response);

                    if ( response ) {
                        switch (response.permission) {
                            case true:
                                if ( this.iconChildren ) {
                                    jQuery( this.iconChildren ).attr('class', 'fa fa-check').attr('style', 'color: green;font-size: large');
                                    jQuery( this.iconChildren ).siblings('input').val('allowed');
                                }

                                break;
                            case false:
                                if ( this.iconChildren ) {
                                    jQuery( this.iconChildren ).attr('class', 'fa fa-close').attr('style', 'color: red;font-size: large');
                                    jQuery( this.iconChildren ).siblings('input').val('denied');
                                }
                                break;
                            default:
                                alert('NOT DEFINED');
                        }
                    } else if (response.error) {
                        alert(response.error);
                        console.log(response.error);
                    }

                    this.iconChildren = null;

                    // nova chamada.
                    checkPermission(tds);

                },
                error: function (e) {
                    alert("An error occurred: " + e.responseText.message);
                    console.log(e);
                }
            });
        }
    }

    jQuery('a.permission').click(
            function () {

                var objPermission = jQuery(this).children('input');

                iconChildren = jQuery(this).children('i');

                permission = jQuery(objPermission).val();

                // console.log(jQuery(this).attr('name'));
                // console.log(permission);


                switch (permission) {
                    case 'allowed':
                        permission = 'deny';
                        break;
                    case 'denied':
                        permission = 'grant';
                        break;
                    case 'notSynchronize':
                        alert('Action must be synchronized');
                        return;
                        break;
                    default:
                        alert('Permission not identified, reload page or return late !');
                        return;

                }

                var name = jQuery(this).attr('name');

                var _aco = '';
                var _aro = '';
                var _action = '*';

                vetor = eval(name);

                if (vetor.length > 1) {
                    _aro = vetor[0][0];
                    var DS = '';
                    for (var i = 1; i < vetor.length - 1; i++) {
                        _aco += DS + vetor[i][0];
                        DS = '/';
                    }
                    _action = vetor[vetor.length - 1][0];
                }

                // Create URL dynamic
                var _url = jQuery('#urlSetPermission').val();

                jQuery.ajax({
                    type: 'post',
                    url: _url,
                    context: { iconChildren : iconChildren },
                    beforeSend: function (xhr) {
                        xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
                    },
                    data: {
                        aro: _aro,
                        aco: _aco,
                        action: _action,
                        permission: permission
                    },
                    dataType: 'json',
                    headers: {
                        'X-CSRF-TOKEN': csrfToken
                    },
                    success: function (response) {

                        // alert((response));

                        if (response.result) {
                            switch (response.result.permission) {
                                case 'allowed':
                                    if ( this.iconChildren ) {
                                        jQuery( this.iconChildren ).attr('class', 'fa fa-check').attr('style', 'color: green;font-size: large');
                                        jQuery( this.iconChildren ).siblings('input').val('allowed');
                                    }

                                    break;
                                case 'denied':
                                    if ( this.iconChildren ) {
                                        jQuery( this.iconChildren ).attr('class', 'fa fa-close').attr('style', 'color: red;font-size: large');
                                        jQuery( this.iconChildren ).siblings('input').val('denied');
                                    }

                                    break;
                                default:
                                    alert('NOT DEFINED');
                            }
                        } else if (response.error) {
                            alert(response.error);
                            console.log(response.error);
                        }
                        this.iconChildren = null;
                    },
                    error: function (e) {
                        alert("An error occurred: " + e.responseText.message);
                        iconChildren = null;
                        console.log(e);
                    }
                });

            }
    );


    jQuery('.description').on('click', function () {

        var _popover = jQuery(this);

        var attr = jQuery.parseJSON(jQuery(this).attr('data-attr'));

        // Create URL dynamic
        var _url = jQuery('#urlGetDescription').val();

        jQuery.ajax({
            type: 'post',
            url: _url,
            context: {popover: _popover, action: attr.action !== undefined ? attr.action : ''},
            beforeSend: function (xhr) {
                xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
            },
            data: {
                controller: attr.controller !== undefined ? attr.controller : '',
                action: attr.action !== undefined ? attr.action : '',
                plugin: attr.plugin !== undefined ? attr.plugin : '',
            },
            dataType: 'json',
            headers: {
                'X-CSRF-TOKEN': csrfToken
            },
            success: function (response) {

                jQuery(this.popover).popover({
                    html: true,
                    title: '<b>Action ' + this.action + '</b> <a href="#" onclick="jQuery(this).parents().popover(\'hide\');" class="close">&times;</a>',
                    content: response.message
                });

                jQuery(this.popover).popover("show");

            },
            error: function (e) {
                alert("An error occurred: " + e.responseText.message);
                console.log(e);
            }
        });


    });



});
