
function menu() {
    var positionFixed = $("#sidebar").css("position") === "fixed";

    var sidebarHovered = false;

    var mobileWidth = 768;

    var toggleSidebar = function () {
        var width = $(window).width();
        var visivel = !$("#sidebar").is(":visible");
        $("#sidebar").toggle('toggle');
        localStorage.setItem("sidebar_visible", visivel);
        if ($('.content').hasClass('full')) {
            if (width >= mobileWidth) {
                $('.content').removeClass('full', {duration: '400'});
            }
        } else {
            $('.content').addClass('full', {duration: '400'});
        }
    };

    var positionSubmenu = function () {
        if ($(window).width() >= mobileWidth) {
            $("#sidebar > .list-group > a[data-submenu]").each(function (index, item) {
                var scroll = (positionFixed) ? 0 : $("body").scrollTop();
                var position = $(item).offset();
                var width = $(item).outerWidth();
                var top = position.top - scroll;
                var left = position.left + width;
                var submenu = $(item).attr('data-submenu');
                $("#" + submenu).css('top', top + 'px').css('left', left + 'px');
            });
        }
    };

    var toggleSubmenu = function (event, self) {
        var submenu = $(self).attr('data-submenu');
        var submenuVisible = $('#' + submenu).is(":visible");
        if (submenu) {
            positionSubmenu();
            if (submenuVisible) {
                $('#' + submenu).hide();
            } else {
                $('#' + submenu).show();
            }
        } else {
            if ($(window).width() < mobileWidth) {
                toggleSidebar();
            }
        }

        event.stopPropagation();
    };

    var openSubmenu = function () {
        $('#sidebar > .list-group > a[data-submenu]').on('mouseenter focusin', function (event) {
            $('.subitem:visible').hide();
            var submenu = $(this).attr('submenu');
            $(this).addClass("active");
            if (!$("#" + submenu).is(":visible")) {
                toggleSubmenu(event, this);
            }
        });
    };

    var closeSubmenu = function () {
        $('#sidebar > .list-group > a[data-submenu]').on('mouseleave focusout', function (event) {
            var submenu = $(this).attr('submenu');
            $(this).removeClass("active");
            if ($("#" + submenu).is(":visible") && !$("#" + submenu).is(":hover")) {
                toggleSubmenu(event, this);
            }
        });
    };

    var observeMenu = function () {
        var width = $(window).width();
        if (width < mobileWidth) {
            $('#sidebar > .list-group > a').on('click', function (event) {
                toggleSubmenu(event, this)
            });
        } else {
            openSubmenu();

            closeSubmenu();

            $('.submenu').on('mouseleave focusout', function () {
                $(this).hide();
            });
        }

        $('#slide-submenu').on('click', toggleSidebar);
    };

    var clickOnSubitem = function () {
        $('.subitem.in').removeClass('in');
        $('.active').removeClass('active');
        $('#sidebar > .list-group > a.active').removeClass('active');
        $('.subitem:visible').hide();

        if ($(window).width() < mobileWidth) {
            toggleSidebar();
        }
    };

    var hiddeMenuOnClickInBody = function () {
        $("#sidebar").hover(function () {
            sidebarHovered = true;
        }, function () {
            sidebarHovered = false;
        });

        $("body").click(function () {
            if (!sidebarHovered) {
                $('#sidebar > .list-group > a.active').removeClass('active');
                $('.subitem:visible').hide();
            }
        });
    };

    var init = function () {
        $(observeMenu());

        $('.subitem a.list-group-item').on('click', clickOnSubitem);

        var width = $(window).width();
        if (width < mobileWidth) {
            localStorage.setItem("sidebar_visible", false);
        }

        if (localStorage.getItem("sidebar_visible") === null) {
            localStorage.setItem("sidebar_visible", true);
        }

        if (localStorage.getItem("sidebar_visible") === 'true') {
            $("#sidebar").show();
        } else {
            $("#sidebar").hide();
            $('.content').addClass('full');
        }
        hiddeMenuOnClickInBody();
    };

    $(document).ready(init);
};

/*
 * FUNTION TO SHOW HELP
*/
function showHelper(controller, action){

	$.ajax({
                    url: urlHelper + '/' + controller + '/' + action ,
                    dataType: "html",                    
                    type: "POST",
                    success: function (retorno ) {

                        if (retorno) {
                            //showErrors(form, retorno.erros );
                            BootstrapDialog.show({
                                type: BootstrapDialog.TYPE_INFO,
				size: BootstrapDialog.SIZE_WIDE,
                                title: 'Helper',
                                message: retorno ,
                                buttons: [{
                                        label: 'Close',
                                        action: function (dialogItself) {
                                            dialogItself.close();
                                        }
                                    }]
                            });                            
                            return;
                        }
                        
                        

                            
                    },
                    error: function (e) {
                        console.log(e);
                         BootstrapDialog.show({
                            type: BootstrapDialog.TYPE_DANGER,
                            title: 'Warnign',
                            message: 'Fail not indentify.' + e,
                            buttons: [{
                                    label: 'Close',
                                    action: function (dialogItself) {
                                        dialogItself.close();
                                    }
                                }]
                        });
                    }
        });
}

menu();