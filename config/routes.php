<?php

use Cake\Routing\Route\DashedRoute;
use Cake\Routing\RouteBuilder;

return function (RouteBuilder $routes) {
    $routes->plugin(
        'AclManager',
        ['path' => '/admin'],
        function (RouteBuilder $routes) {
            $routes->setRouteClass(DashedRoute::class);
        });
};
