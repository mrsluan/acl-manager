# AclManager plugin for CakePHP

## Installation

You can install this plugin into your CakePHP application using [composer](https://getcomposer.org).

The recommended way to install composer packages is:

```
composer require mrsilva/acl-manager
```

Then in your `src/Application.php`:
```php
public function bootstrap()
{
    parent::bootstrap();


    // Load more plugins here
    $this->addPlugin("Acl");
    $this->addPlugin('AclManager',['bootstrap' => false, 'routes' => true]);
}
```

## Creating tables

To create the ACL requires tables, run the following `Migrations` command:

```
bin/cake migrations migrate -p Acl
```

## Accessing Admin Page

Add address your page the url

```
https://..../admin/dashboards
```

Into dashboards the Plugin System to guide to do configure nescessary.
Not forget set database!!!


## Table of version and requirements

Plugin version | Branch | CakePHP version           | PHP minimum version |
--- |--------|---------------------------|---------------------|
3.x | cake4  | >=4.0.0 <5.0.0 | >=8.1               |
2.x | cake3  | >=3.8.0 <4.0.0 | >=7.1               |
1.x | cake3  | >=3.3.0 <3.8.0 | >=5.6               |
